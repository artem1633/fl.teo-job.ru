<?php

namespace app\models;

use Yii;
use app\base\AppActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "resume_status".
 *
 * @property int $id
 * @property string $name
 * @property string $icon
 */
class ResumeStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resume_status';
    }

    public static function find()
    {
        if(Yii::$app->user->identity->type != 0){
            $user_id = Yii::$app->user->identity->id;
        } else {
            $user_id = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'user_id' => $user_id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0)
            {
                $user_id = Yii::$app->user->identity->id;
                if($model->user_id != $user_id){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['user_id'], 'integer'],
            [['name', 'icon'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'icon' => 'Иконка',
            'user_id' => 'Компания',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->identity->id;
        }

        return parent::beforeSave($insert);
    }

    public function setDefaultValues()
    {
        $status = new ResumeStatus();
        $status->name = 'Переписка';
        $status->icon = 'fa-file-text';
        $status->save();

        $status = new ResumeStatus();
        $status->name = 'Тестовое задание';
        $status->icon = 'fa-anchor';
        $status->save();

        $status = new ResumeStatus();
        $status->name = 'На испытательном';
        $status->icon = 'fa-dollar';
        $status->save();

        $status = new ResumeStatus();
        $status->name = 'Не отвечает';
        $status->icon = 'fa-thumbs-down';
        $status->save();

        $status = new ResumeStatus();
        $status->name = 'Не сделал тест';
        $status->icon = 'fa-thumbs-down';
        $status->save();

        $status = new ResumeStatus();
        $status->name = 'Отложил';
        $status->icon = 'fa-bitbucket';
        $status->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResumes()
    {
        return $this->hasMany(Resume::className(), ['status_id' => 'id']);
    }
}
