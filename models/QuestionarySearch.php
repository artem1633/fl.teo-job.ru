<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Questionary;

/**
 * QuestionarySearch represents the model behind the search form about `app\models\Questionary`.
 */
class QuestionarySearch extends Questionary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'vacancy_id', 'user_id', 'active', 'count', 'filling_count', 'is_template', 'publish_answer'], 'integer'],
            [['name', 'description', 'link', 'date_cr', 'date_up', 'access', 'show_in_desktop', 'publish_company'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $is_template)
    {
        if($is_template == null || $is_template == 0) $query = Questionary::find()->where([ 'or', [ 'is_template' => 0], ['is_template'=> null ]]);
        else $query = Questionary::find()->where(['is_template' => $is_template]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'vacancy_id' => $this->vacancy_id,
            'user_id' => $this->user_id,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
            'active' => $this->active,
            'count' => $this->count,
            'filling_count' => $this->filling_count,
            'is_template' => $this->is_template,
            'publish_answer' => $this->publish_answer,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'access', $this->access])
            ->andFilterWhere(['like', 'show_in_desktop', $this->show_in_desktop])
            ->andFilterWhere(['like', 'publish_company', $this->publish_company]);

        return $dataProvider;
    }
}
