<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "questions".
 *
 * @property int $id
 * @property int $questionary_id Анкета
 * @property string $name Наименование
 * @property string $description Описание
 * @property string $foto Картинка
 * @property int $require Обязательное или нет
 * @property int $ordering Сортировка
 * @property int $view_in_table Выводить в таблицу информацию
 * @property int $type Тип вопроса
 * @property string $text Текст
 * @property double $number Число
 * @property string $date Дата
 * @property string $individual Выбор одного варианта
 * @property string $multiple Выбор несколько вариантов
 *
 * @property Questionary $questionary
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }
    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['questionary_id', 'require', 'ordering', 'view_in_table', 'type', 'general_access'], 'integer'],
            [['description', 'foto', 'text', 'individual', 'multiple'], 'string'],
            [['number'], 'number'],
            [['date'], 'safe'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',],
            [['name', 'avatar'], 'string', 'max' => 255],
            [['questionary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questionary::className(), 'targetAttribute' => ['questionary_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questionary_id' => 'Анкета',
            'name' => 'Наименование',
            'description' => 'Описание',
            'foto' => 'Картинка',
            'require' => 'Обязательное',
            'ordering' => 'Сортировка',
            'view_in_table' => 'Выводить в таблицу информацию',
            'type' => 'Тип ответа',
            'text' => 'Текст',
            'number' => 'Число',
            'date' => 'Дата',
            'individual' => 'Выбор одного варианта',
            'multiple' => 'Выбор несколько вариантов',
            'avatar' => 'Аватар',
            'general_access' => 'Скрыть поле',
            'file' => 'Картинка',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord){
            $new = Questions::find()->where(['questionary_id' => $this->questionary_id])->orderBy(['ordering' => SORT_DESC])->one();
            if($new == null) {
                $this->ordering = $this->getLastId();
            }else{
                $this->ordering = $new->ordering + 1;
            }
        }
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        Directory::deleteDirectory($this->id, 'questions');
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionary()
    {
        return $this->hasOne(Questionary::className(), ['id' => 'questionary_id']);
    }

    //Получить последнего ид
    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'questions'")
            ->one();
        if($res) return $res["AUTO_INCREMENT"];
    }

    public function getTypeList()
    {
        return [
            1 => 'Текст',
            2 => 'Число',
            3 => 'Выбор одного варианта',
            4 => 'Выбор несколько вариантов',
            5 => 'Дата',
            7 => 'Текст большой',
            8 => 'Картинки',
        ];
    }

    public function getTypeDescription()
    {
        switch ($this->type) {
            case 0: return 'ФИО';
            case 1: return 'Текст';
            case 2: return 'Число';
            case 3: return 'Выбор одного варианта';
            case 4: return 'Выбор несколько вариантов';
            case 5: return 'Дата';
            case 6: return 'Аватар';
            case 7: return 'Текст большой';
            case 8: return 'Картинки';
            default: return 'Неизвестно';
        }
    }

    public function getTypeCode()
    {
        switch ($this->type) {
            case 0: return 'default';
            case 1: return 'textarea';
            case 2: return 'number';
            case 3: return 'radio';
            case 4: return 'checkbox';
            case 5: return 'date';  
            case 6: return 'avatar';
            case 7: return 'bigtextarea';         
            default: return 'Неизвестно';
        }
    }

    public function getColumnsList($questionary_id)
    {
        $active = [];
        $questions = Questions::find()->where(['questionary_id' => $questionary_id])->orderBy(['ordering' => SORT_ASC])->all();
        $i = 0;
        foreach ($questions as $question) {
            $i++;
            $active += [
                $question->id => ['content' => '<i class="glyphicon glyphicon-cog"></i> #' . $i . '&nbsp;' . $question->name ],
            ];
        }

        return $active;
    }

    //Получить данные блока
    public function getItemValues()
    {
        $image_path = '';
        foreach (json_decode($this->foto) as $image) {
            $path = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $image->path;
                /*echo "<pre>";
                print_r($path);
                echo "</pre>";*/
            $image_path .= ' <img style="width:80px;height:80px;" src="' . $path . '" >';
        }

        if($this->type == 3) {
            $text = '<div class="row">
                    <div class="col-md-12">
                        '. $image_path.'
                    </div>
                    <div class="col-md-12">
                        <label class="col-md-12 control-label">' . $this->description .'</label>
                    </div>
                    <br>'.$this->getIndividual();
        }
        else {
            if($this->type == 4) {
                $text = '<div class="row">
                    <div class="col-md-12">
                        '. $image_path.'
                    </div>
                    <div class="col-md-12">
                        <label class="col-md-12 control-label">' . $this->description .'</label>
                    </div>
                    <br>'.$this->getMultiple();
            }
            else {
                if($this->type == 6){
                    $text = '<div class="row">
                        <div class="col-md-12">
                            <img style="width:100px;height:100px;" class="img-circle" src="' . 'http://' . $_SERVER['SERVER_NAME'] . '/images/nouser.png' . '" >
                        </div>
                        <div class="col-md-12">
                            <label class="col-md-12 control-label">' . $this->description .'</label>
                        </div>
                    </div>'; 
                }
                else {
                    $text = '<div class="row">
                        <div class="col-md-12">
                            '. $image_path.'
                        </div>
                        <div class="col-md-12">
                            <label class="col-md-12 control-label">' . $this->description .'</label>
                        </div>
                        <div class="col-md-12"> 
                            <input type="text" class="form-control" style="margin-top:10px;" /> 
                        </div>
                    </div>';
                }
            }         
        }

        $button = Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/questions/delete','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/questions/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ]);

        $name = '
            <div class="grid-item" style="text-align:center;">
                '. 
                ($this->type != 0 /*&& $this->type != 6*/ ? $button : '')
                .   $this->name . $text . 
            '</div>';

        return $name;
    }

    public function getIndividual()
    {
        $result = '';
        $individual = 0;
        foreach (json_decode($this->individual) as $value) {
            $individual++;
            if($individual == 1){
                $result .='
                    <div class="row">
                        <div class="col-md-1 col-sm-1 col-xs-1"></div>
                        <div class="col-md-9 col-sm-9 col-xs-9">Варианты</div>
                        <div class="col-md-2 col-sm-2 col-xs-2">Балл</div> 
                    </div>';
            }
            $result .= '<div class="row" id="row<?=$individual?>">
                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <input type="radio" disabled name="individualValue[]" value="'.$individual.'" style="margin-top:15px; width:20px;height:20px;" >
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9"> 
                        <input type="text" class="form-control" name="individual[]" value="'.$value->value.'" style="margin-top:10px;" /> 
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2"> 
                        <input type="number" class="form-control" name="ball_indiv[]" value="'.$value->ball.'" style="margin-top:10px;" /> 
                    </div>
                </div>
                ';
        }

        return $result;
    }

    public function getMultiple()
    {
        $result = '';
        $multiple = 0;
        foreach (json_decode($this->multiple) as $value) {
            $multiple++;
            if($multiple == 1){
                $result .='
                    <div class="row">
                        <div class="col-md-1 col-sm-1 col-xs-1"></div>
                        <div class="col-md-9 col-sm-9 col-xs-9">Варианты</div>
                        <div class="col-md-2 col-sm-2 col-xs-2">Балл</div> 
                    </div>';
            }
            $result .= '<div class="row" id="row<?=$multiple?>">
                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <input type="checkbox" disabled name="multipleValue[]" ' . ' value="'.$multiple.'" style="margin-top:15px; width:25px;height:25px;" >
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9"> 
                        <input type="text" class="form-control" name="multiple[]" value="'.$value->question.'" style="margin-top:10px;" /> 
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2"> 
                        <input type="number" class="form-control" name="ball_multiple[]" value="'.$value->ball.'" style="margin-top:10px;" /> 
                    </div>
                </div>
                ';
        }

        return $result;
    }

    //Получить данные блока
    public function getSelectesItem($value, $ball_for_question)
    {
        $result = '';
        if($ball_for_question != null) $text = '<i style="color:red;font-size:14px;" class="pull-right">Балл : '.$ball_for_question.'</i>';
        else $text = '';
        if($this->type == 1 || $this->type == 0){
            $result .='
            <div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name . '</h3></label>
                <div class="col-md-12">
                    <textarea class="form-control" rows="1">'.$value.'</textarea>
                 </div>
            </div>';
        }

        if($this->type == 2){
            $result .='
            <div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name .'</h3></label>
                <div class="col-md-12">
                    <input type="number" value="'.$value.'" class="form-control">
                 </div>
            </div>';
        }

        if($this->type == 3){
            $ball = 0;
            foreach (json_decode($this->individual) as $individual) {
                $checked = '';
                foreach (json_decode($value) as $val) {
                    if($val->key == $individual->value) {
                        $checked = 'checked="checked"';
                        $ball = $individual->ball;
                    }
                }

                $result .='
                    <div class="row">
                        <div class="col-md-1">
                            <input type="radio" '.$checked.' disabled="disabled" style="width:20px;height:20px;" >
                        </div>
                        <label style="margin-top: 5px;" class="col-md-11 control-label">'.$individual->value.'</label>
                    </div>
                ';

            }

            $result ='<div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name .'<i style="color:red;font-size:14px;" class="pull-right">Балл : '.$ball.'</i></h3></label>' . $result;
            $result .=' </div>';
        }

        if($this->type == 4){
            $ball = 0;
            foreach (json_decode($this->multiple) as $multiple) {
                $checked = '';
                foreach (json_decode($value) as $val) {
                    if($val->key == $multiple->question) {
                        $checked = 'checked="checked"';
                        $ball += $multiple->ball;
                    }
                }

                $result .='
                    <div class="row">
                        <div class="col-md-1">
                            <input type="checkbox" '.$checked.' disabled="disabled" style=" width:20px;height:20px;" >
                        </div>
                        <label style="margin-top: 5px;" class="col-md-11 control-label">'.$multiple->question.'</label>
                    </div>
                ';
            }
            $result ='<div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name .'<i style="color:red;font-size:14px;" class="pull-right">Балл : '.$ball.'</i></h3></label>' . $result;
            $result .=' </div>';
        }

        if($this->type == 5){
            $result .='
            <div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name . '</h3></label>
                <div class="col-md-12">
                    <input type="text" value="'.$value.'" class="form-control">
                 </div>
            </div>';
        }

        if($this->type == 6){
            if($this->avatar == null) $path = 'http://' . $_SERVER['SERVER_NAME'] . '/images/nouser.png';
            else $path = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $this->avatar;

            $result .='
            <div class="row">
                <center><img style="width: 100px;border-radius: 1em;border: solid 1px #cecece;" src="' . $path . '" ></center>
            </div>';
        }

        if($this->type == 7){
            $result .='
            <div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name . '</h3></label>
                <div class="col-md-12">
                    <textarea class="form-control" rows=6>'.$value.'</textarea>
                 </div>
            </div>';
        }

        if($this->type == 8){
            $images = '';
            foreach (json_decode($value) as $image) {
                $path = 'http://' . $_SERVER['SERVER_NAME'] . '/uploads/images/' . $image->image_name;
                $images .= '<img style="width: 100px;height: 100px;border-radius: 1em;border: solid 1px #cecece;" src="' . $path . '" >';
            }

            $result .='
            <div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name . '</h3></label>
                <center>'.$images.'</center>
            </div>';
        }

        return $result;
    }

    public function getSelectesItemPrint($value, $ball_for_question)
    {
        $result = '';
        if($ball_for_question != null) $text = '<i style="color:red;font-size:14px;" class="pull-right">Балл : '.$ball_for_question.'</i>';
        else $text = '';
        if($this->type == 1 || $this->type == 0){
            $result .='<label class="col-md-12 control-label">'.$this->name .'</label><div class="col-md-12">
                    <textarea class="form-control" rows="1">'.$value.'</textarea></div>';
        }

        if($this->type == 2){
            $result .='<label class="col-md-12 control-label">'.$this->name .'</label><div class="col-md-12">
                    <input type="number" '.$value.' class="form-control"></div>';
        }

        if($this->type == 3){
            $ball = 0;
            foreach (json_decode($this->individual) as $individual) {
                $checked = '';
                foreach (json_decode($value) as $val) {
                    if($val->key == $individual->value) {
                        $checked = 'checked="checked"';
                        $ball = $individual->ball;
                    }
                }


                $result .='
                            <div class="col-md-1">
                            <input type="radio" '.$checked.' disabled="disabled" style="width:20px;height:20px;" >
                            <label style="margin-top: 5px;" class="col-md-11 control-label">   '.$individual->value.'</label>
                            </div>';

            }

            $result ='<label class="col-md-12 control-label">'. $this->name .'</label>' . $result;
        }

        if($this->type == 4){
            $ball = 0;
            foreach (json_decode($this->multiple) as $multiple) {
                $checked = "&nbsp;&nbsp;";
                foreach (json_decode($value) as $val) {
                    if($val->key == $multiple->question) {
                        $checked = 'X';
                        $ball += $multiple->ball;
                    }
                }

                $result .='<div class="col-md-12"><span  style="display: block; width:20px;height:20px; border: 1px solid black;" >&nbsp;'.$checked.
                    '&nbsp;</span><label style="margin-top: 5px;" class="col-md-11 control-label">  '.$multiple->question.'</label></div>';
            }
            $result ='<label class="col-md-12 control-label">'. $this->name .'</label>' . $result;
        }

        if($this->type == 5){
            $result .='<label class="col-md-12 control-label">'.$this->name .'</label><div class="col-md-12">
                    <input type="text" '.$value.' class="form-control"></div>';
        }
        return $result;
    }

    //Получить данные блока
    public function getQuestions($value, $ball_for_question, $resume)
    {
        $result = '';
        if($ball_for_question != null) $text = '<i style="color:red;font-size:14px;" class="pull-right">Балл : '.$ball_for_question.'</i>';
        else $text = '';
        if($this->type == 1 || $this->type == 0){
            $result .='
            <div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name . '</h3></label>
                <div class="col-md-12">
                    <textarea class="form-control" disabled="true" style="color:black;" rows="1">'.$value.'</textarea>
                 </div>
            </div>';
        }

        if($this->type == 2){
            $result .='
            <div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name .'</h3></label>
                <div class="col-md-12">
                    <input type="number" disabled="true" style="color:black;" value="'.$value.'" class="form-control">
                 </div>
            </div>';
        }

        if($this->type == 3){
            foreach (json_decode($this->individual) as $individual) {
                $checked = '';
                foreach (json_decode($value) as $val) {
                    if($val->key == $individual->value) {
                        $checked = 'checked="checked"';
                    }
                }
                $result .='
                    <div class="row">
                        <div class="col-md-1">
                            <input type="radio" '.$checked.' disabled="disabled" style="width:20px;height:20px;" >
                        </div>
                        <label style="margin-top: 5px;" class="col-md-11 control-label">'.$individual->value.'</label>
                    </div>
                ';
            }
            $result ='<div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name .'</h3></label>' . $result;
            $result .=' </div>';
        }

        if($this->type == 4){
            foreach (json_decode($this->multiple) as $multiple) {
                $checked = '';
                foreach (json_decode($value) as $val) {
                    if($val->key == $multiple->question) {
                        $checked = 'checked="checked"';
                    }
                }
                $result .='
                    <div class="row">
                        <div class="col-md-1">
                            <input type="checkbox" '.$checked.' disabled="disabled" style=" width:20px;height:20px;" >
                        </div>
                        <label style="margin-top: 5px;" class="col-md-11 control-label">'.$multiple->question.'</label>
                    </div>
                ';
            }
            $result ='<div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name .'</h3></label>' . $result;
            $result .=' </div>';
        }

        if($this->type == 5){
            $result .='
            <div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name . '</h3></label>
                <div class="col-md-12">
                    <input type="text" disabled="true" style="color:black;" value="'.$value.'" class="form-control">
                 </div>
            </div>';
        }

        if($this->type == 6){
            if($resume->avatar == null) $path = 'http://' . $_SERVER['SERVER_NAME'] . '/images/nouser.png';
            else $path = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $resume->avatar;

            $result .='
            <div class="row">
                <center><img style="width: 100px;border-radius: 1em;border: solid 1px #cecece;" src="' . $path . '" ></center>
            </div>';
        }

        if($this->type == 7){
            $result .='
            <div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name . '</h3></label>
                <div class="col-md-12">
                    <textarea class="form-control" disabled="true" style="color:black;" rows="6">'.$value.'</textarea>
                 </div>
            </div>';
        }

        if($this->type == 8){
            $images = '';
            foreach (json_decode($value) as $image) {
                $path = 'http://' . $_SERVER['SERVER_NAME'] . '/uploads/images/' . $image->image_name;
                $images .= '<img style="width: 100px;height: 100px;border-radius: 1em;border: solid 1px #cecece;" src="' . $path . '" >';
            }

            $result .='
            <div class="row">
                <label class="col-md-12 control-label"><h3>'. $this->name . '</h3></label>
                <center>'.$images.'</center>
            </div>';
        }
        
        return $result;
    }
}
