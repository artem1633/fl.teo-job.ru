<?php

namespace app\models;

use Yii;
use app\base\AppActiveQuery;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property int $user_id Компания/Пользователь
 * @property string $name Наименование
 * @property string $nik Ник
 * @property string $email Почта
 * @property string $skype Скайп
 * @property string $additional Дополнительная информация
 * @property string $link Ссылка на профиль
 * @property string $phone Телефон
 *
 * @property Users $user
 * @property Resume[] $resumes
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    public static function find()
    {
        if(Yii::$app->user->identity->type != 0){
            $user_id = Yii::$app->user->identity->id;
        } else {
            $user_id = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'user_id' => $user_id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0)
            {
                $user_id = Yii::$app->user->identity->id;
                if($model->user_id != $user_id){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['additional'], 'string'],
            [['name', 'nik', 'email', 'skype', 'link', 'phone'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Компания/Пользователь',
            'name' => 'Наименование',
            'nik' => 'Ник',
            'email' => 'Почта',
            'skype' => 'Скайп',
            'additional' => 'Дополнительная информация',
            'link' => 'Ссылка на профиль',
            'phone' => 'Телефон',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->identity->id;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResumes()
    {
        return $this->hasMany(Resume::className(), ['client_id' => 'id']);
    }
}
