<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lessons_users".
 *
 * @property int $id
 * @property int $lessons_group Номер групи
 * @property int $user_id Пользователь
 * @property int $passed Пройдено
 *
 * @property Users $user
 */
class LessonsUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lessons_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lessons_group', 'user_id', 'passed'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lessons_group' => 'Номер групи',
            'user_id' => 'Пользователь',
            'passed' => 'Пройдено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public static function getLessonsStatus($group) {
        $lessons = static::find()->where(['user_id'=>Yii::$app->user->id])
            ->andWhere(['lessons_group'=>$group])->one();
        if (!$lessons) {
            $lessons = new self();
            $lessons->lessons_group = $group;
            $lessons->user_id = Yii::$app->user->id;
            $lessons->save();
        }
        return $lessons->passed;
    }

    public static function setPassed($group) {
        $lessons = static::find()->where(['user_id'=>Yii::$app->user->id])
            ->andWhere(['lessons_group'=>$group])->one();
        if ($lessons) {
            $lessons->passed = 1;
            $lessons->save();
        }
    }

    public static function resetPassed($group) {
        $lessons = static::find()->where(['user_id'=>Yii::$app->user->id])
            ->andWhere(['lessons_group'=>$group])->one();
        if ($lessons) {
            $lessons->passed = 0;
            $lessons->save();
        }
    }
}
