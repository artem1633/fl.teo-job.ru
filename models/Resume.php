<?php

namespace app\models;

use Yii; 
use app\base\AppActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
 
/**
 * This is the model class for table "resume".
 *
 * @property int $id
 * @property int $group_id Группа
 * @property int $status_id Статус
 * @property string $date_cr Дата и время заполнения
 * @property int $category Список из анкет по категориям
 * @property int $is_new Новая
 * @property string $code Уникальный код
 * @property string $telegram_chat_id Ид чата телеграма
 * @property int $connect_telegram Подключен к телеграмм
 * @property int $new_sms Новое сообщение
 * @property int $vacancy_id
 * @property string $values
 * @property int $correspondence
 * @property string $fio
 * @property string $tags
 * @property int $balls Баллы за ответы
 * @property string $avatar Аватар
 * @property string $ip IP адрес
 * @property int $time_spent Время которое ушло на тест
 * @property int $delivered Сдан или нет
 * @property double $sales Продано
 * @property int $user_id Пользователь
 * @property int $buyed Куплено
 * @property string $description Описание
 * @property double $budget Бюджет
 * @property int $client_id Клиент
 * @property string $link
 * @property string $link_for_project
 * @property int $template_id Шаблон сообщений
 * @property string $text_template Текст сообщений
 *
 * @property Clients $client
 * @property Group $group
 * @property ResumeStatus $status
 * @property SmsTemplate $template
 * @property Users $user
 * @property Vacancy $vacancy
 */
class Resume extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $client_nik;
    public $client_link;
    public $client_additional;
    public static function tableName()
    {
        return 'resume';
    }

    public static function find()
    {
        if(Yii::$app->user->identity->type != 0){
            $user_id = Yii::$app->user->identity->id;
        } else {
            $user_id = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'user_id' => $user_id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0)
            {
                $user_id = Yii::$app->user->identity->id;
                if($model->user_id != $user_id){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public $newQuestionary;
    public $newLink;
    public function rules()
    {
        return [
            [[ 'category', 'is_new', 'connect_telegram', 'new_sms', 'correspondence', 'balls', 'time_spent', 'delivered', 'user_id', 'buyed', 'newQuestionary', 'template_id'], 'integer'],
            [['date_cr'], 'safe'],
            [['values', 'description', 'text_template', 'client_additional'], 'string'],
            [['code'], 'unique'],
            [['sales', 'budget'], 'number'],
            [['code', 'telegram_chat_id', 'fio', 'avatar', 'ip', 'link_for_project', 'client_nik', 'client_link'], 'string', 'max' => 255],
            ['group_id', 'validateGroup'],
            ['status_id', 'validateStatus'],
            ['vacancy_id', 'validatVacancy'],
            ['client_id', 'validateClient'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => SmsTemplate::className(), 'targetAttribute' => ['template_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    { 
        return [ 
            'id' => 'ID',
            'group_id' => 'Группа',
            'status_id' => 'Статус',
            'date_cr' => 'Дата и время заполнения',
            'category' => 'Список из анкет по категориям',
            'is_new' => 'Новая',
            'code' => 'Поделить тест',
            'telegram_chat_id' => 'Ид чата телеграма',
            'connect_telegram' => 'Телеграмм',
            'new_sms' => 'Новое сообщение',
            'vacancy_id' => 'Тип',
            'values' => 'Значение',
            'correspondence' => 'Переписка',
            'fio' => 'Название',
            'tags' => 'Теги',
            'balls' => 'Баллы за ответы',
            'avatar' => 'Аватар',
            'ip' => 'Ip адрес',
            'time_spent' => 'Время которое ушло на тест',
            'delivered' => 'Сдан или нет',
            'sales' => 'Продано',
            'user_id' => 'Пользовател / Компания',
            'buyed' => 'Куплено',
            'newQuestionary' => 'Тесты',
            'newLink' => 'Ссылка новая теста',
            'description' => 'Описание',
            'budget' => 'Бюджет',
            'client_id' => 'Клиент',
            'link_for_project' => 'Ссылка на проект',
            'client_nik' => 'Ник',
            'client_link' => 'Ссылка',
            'template_id' => 'Шаблон сообщений',
            'text_template' => 'Текст сообщений',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->date_cr = date('Y-m-d H:i:s');
            $this->connect_telegram = 0;
            $this->user_id = Yii::$app->user->identity->id;

            $length = 10;
            $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

            do {
                $string ='';
                for( $i = 0; $i < $length; $i++) {
                    $string .= $chars[rand(0,strlen($chars)-1)];
                }
            } while ( Resume::find()->where(['code' => $string])->one() != null );

            $this->code = $string;

        }

        return parent::beforeSave($insert);
    }

    public function getColorCode()
    {
        $length = 6;
        $chars ="abcdef1234567890";
        $string ='';
        for( $i = 0; $i < $length; $i++) {
            $string .= $chars[rand(0,strlen($chars)-1)];
        }
        return '#' . $string;
    }

    public function getTelegram()
    {
        return [
            1 => 'Да',
            0 => 'Нет',
        ];
    }


    public function getTelegramDescription()
    {
        if($this->connect_telegram == 1) return 'Да';
        else return 'Нет';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(ResumeStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tags::className(), ['id' => 'tag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(SmsTemplate::className(), ['id' => 'template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacancy()
    {
        return $this->hasOne(Vacancy::className(), ['id' => 'vacancy_id']);
    }

    //Получить описание типов пользователя.
    public function getNewDescription()
    {
        switch ($this->is_new) {
            case 1: return "Да";
            case 0: return "Нет";
            default: return "<i style='color:red'>(Не задано)</i>";
        }
    }

    //Получить описание типов пользователя.
    public function getConnectedDescription()
    {
        switch ($this->connect_telegram) {
            case 1: return "Да";
            case 0: return "Нет";
            default: return "<i style='color:red'>(Не задано)</i>";
        }
    }

    //Получить описание типов пользователя.
    public function getNewSmsDescription()
    {
        switch ($this->new_sms) {
            case 1: return "Да";
            case 0: return "Нет";
            default: return "<i style='color:red'>(Не задано)</i>";
        }
    }

    //Получить описание типов пользователя.
    public function getCorrespondence()
    {
        switch ($this->correspondence) {
            case 1: return "Да";
            case 0: return "Нет";
            default: return "<i style='color:red'>(Не задано)</i>";
        }
    }

    public function getGroupList()
    {
        $group = Group::find()->all();
        return ArrayHelper::map($group, 'id', 'name');
    }

    public function getStatusList()
    {
        $status = ResumeStatus::find()->all();
        return ArrayHelper::map($status, 'id', 'name');
    }

    public function getVacancyList()
    {
        $vacancy = Vacancy::find()->all();
        return ArrayHelper::map($vacancy, 'id', 'name');
    }

    public function getTagsList()
    {
        $tags = Tags::find()->all();
        return ArrayHelper::map($tags, 'id', 'name');
    }

    public function getQuestionaryList()
    {
        $questionary = Questionary::find()->all();
        return ArrayHelper::map($questionary, 'id', 'name');
    }

    public function getClientList()
    {
        $clients = Clients::find()->all();
        return ArrayHelper::map($clients, 'id', 'name');
    }

    public function getTemplateList()
    {
        $template = SmsTemplate::find()->all();
        return ArrayHelper::map($template, 'id', 'name');
    }

    public function getResumeList()
    {
        $resume = Resume::find()->all();
        $result = [];
        foreach ($resume as $value) {
            $result [] = [
                'id' => $value->id,
                'title' => 'ФИО=' . $value->fio . ', ID=' . $value->id,
            ];
        }

        return ArrayHelper::map($result, 'id', 'title');
    }


    public function getConnectList()
    {
        return [
            0 => 'Нет',
            1 => 'Да',
        ];
    }

    public function getNewSmsList()
    {
        return [
            0 => 'Нет',
            1 => 'Да',
        ];
    }

    public function getCorrespondenceList()
    {
        return [
            0 => 'Нет',
            1 => 'Да',
        ];
    }

    public function validateClient($attribute, $params)
    {
        $client = Clients::find()->where(['id' => $this->client_id])->one();
        if (!isset($client))
        {
            $client = new Clients();
            $client->name = $this->client_id;
            $client->nik = $this->client_nik;
            $client->link = $this->client_link;
            $client->additional = $this->client_additional;
            
            if ($client->save())
            {
                $this->client_id = $client->id;
            }
            else
            {
                $this->addError($attribute,"Не создано новый клиент");
            }
        }
        else{
            $client->nik = $this->client_nik;
            $client->link = $this->client_link;
            $client->additional = $this->client_additional;
            $client->save();
        }
    }

    //Новая группа
    public function validateGroup($attribute, $params)
    {
        $group = Group::find()->where(['id' => $this->group_id])->one();
        if (!isset($group))
        {
            $group = new Group();
            $group->name = $this->group_id;
            
            if ($group->save())
            {
                $this->group_id = $group->id;
            }
            else
            {
                $this->addError($attribute,"Не создано новая группа");
            }
        }
    }

    //Новый статус
    public function validateStatus($attribute, $params)
    {
        $status = ResumeStatus::find()->where(['id' => $this->status_id])->one();
        if (!isset($status))
        {
            $status = new ResumeStatus();
            $status->name = $this->status_id;
            
            if ($status->save())
            {
                $this->status_id = $status->id;
            }
            else
            {
                $this->addError($attribute,"Не создано новый статус");
            }
        }
    }

    //Новая вакансия
    public function validatVacancy($attribute, $params)
    {
        $vacancy = Vacancy::find()->where(['id' => $this->vacancy_id])->one();
        if (!isset($vacancy))
        {
            $vacancy = new Vacancy();
            $vacancy->name = $this->vacancy_id;
            
            if ($vacancy->save())
            {
                $this->vacancy_id = $vacancy->id;
            }
            else
            {
                $this->addError($attribute,"Не создано новая вакансия");
            }
        }
    }

    public function validateTrue($string)
    {
        if( $string =='да' || $string == 'ДА' || $string == 'Да' || $string == 'дА') return 1;
        if( $string =='НЕТ' || $string == 'НЕт' || $string == 'Нет' || $string == 'НеТ' || $string == 'нЕТ' || $string == 'неТ' || $string == 'неТ' || $string == 'нЕт' || $string == 'нет' || $string == 'не' || $string == 'Не' || $string == 'нЕ' || $string == 'НЕ') return 0;
        return $string;
    }

    public function getTime()
    {
        $min = (int)($this->time_spent / 60);
        $sek = $this->time_spent - $min * 60;
        return $min . ' мин. ' . $sek . ' сек.';
    }
}
