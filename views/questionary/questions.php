<?php

use app\models\Lessons;
use app\models\LessonsUsers;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use kartik\sortinput\SortableInput;
use yii\widgets\ActiveForm;

CrudAsset::register($this);
$this->title = 'Анкета';
$this->params['breadcrumbs'][] = $this->title;

if($active == null) $disabled = 'display:none;';
else $disabled = '1';
/* 
echo "q=".$questionary;
echo "<br>user_id=".$user_id;*/
?>
<div class="users-page-index">

    <?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>

    <div class="question-container">
        <div class="row">
            <div class="col-md-7" style="padding-top: 20px;">
                <?php $form = ActiveForm::begin(); ?>
                <div class="panel panel-success panel-hidden-controls" >
                    <div class="panel-heading ui-draggable-handle">
                        <h1 class="panel-title">
                            <b>Форма для заказов</b> &nbsp; &nbsp;
                            <span  style="<?=$disabled?>" >
								    <?= Html::submitButton( 'Сохранить', ['class' => 'btn btn-warning btn-rounded','data-introindex'=>'5-1']) ?>
								</span>
                        </h1>
                        <?= Html::a('Добавить поле <i class="glyphicon glyphicon-plus"></i>', ['/questions/create', 'questionary_id' => $questionary->id], ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-danger btn-rounded pull-right','data-introindex'=>'5-2']) ?>
                    </div>
                    <div class="panel-body">
                        <div style="margin: 10px; max-height: 600px; overflow: auto;">
                            <?= SortableInput::widget([
                                'name'=>'active',
                                'id'=>'actives',
                                'items' => $active,
                                //'value' => '13,14,15,16,17,18',
                                'hideInput' => true,
                                'sortableOptions' => [
                                    'connected'=>true,
                                    'itemOptions'=>['class'=>'', 'style' => 'background-color: #dff0d8;'],
                                ],
                                'options' => ['class'=>'form-control', 'readonly'=>true]
                            ])?>
                        </div>
                    </div>
                    <div class="panel-footer">
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>

            <div class="col-md-5" style="padding-top: 20px;">

                <div <div class="col-md-12">
                    <div class="panel panel-success panel-hidden-controls" >
                        <div class="panel-heading ui-draggable-handle">
                            <h1 class="panel-title"> <b data-introindex="5-3">Настройки</b>

                                <button  type="button" class="btn btn-xs btn-warning" onclick="startLearn();">Обучение</button>
                            </h1>

                            <?= Html::a('Редактировать &nbsp;<i class="glyphicon glyphicon-pencil"></i>', ['/questionary/update', 'id' => $id,], [ 'role'=>'modal-remote','title'=> 'Редактировать','class'=>'btn btn-warning btn-rounded pull-right','data-introindex'=>'5-4']) ?>
                        </div>
                        <div class="panel-body">
                            <div style="margin: 10px; max-height: 600px; overflow: auto;">
                                <p data-introindex="7-1"><b>Ссылка на фоорму:</b> <?= Html::a('<span class="is-hidden-mobile">https://'. $_SERVER['SERVER_NAME'].'/'.$questionary->link .'	</span>', ['/'.$questionary->link], ['data-pjax' => '0','target'=> "_blank"]) ?></p>
                                <p><b>Название</b> : <?=$questionary->name?></p>
                                <p><b>Тип заказа</b> : <?=$questionary->vacancy->name?></p>
                                <?php if($questionary->type == 2) { ?>
                                <?php } ?>
                                <p><b>Количество заполнений</b> : <?=$questionary->filling_count?></p>
                                <p><b>Показывать информацию на раб. столе</b> : <?=$questionary->getShowDesription()?></p>
                                <p><b>Публиковать на страницы компании</b> : <?=$questionary->getPublishDesription()?></p>
                                <p><b>Доступ</b> : <?=$questionary->getAccessDesription()?></p>
                                <p><b>Описание теста</b> : <?=$questionary->description?></p>
                            </div>
                        </div>
                        <div class="panel-footer">
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="padding-top: 30px;">
                </div>
            </div>
        </div>
        <?php Pjax::end() ?>
    </div>

    <?php Modal::begin([
        "id"=>"ajaxCrudModal",
        "options" => [
            "tabindex" => false,
        ],
        "footer"=>"",// always need it for jquery plugin
    ])?>
    <?php Modal::end(); ?>
    <?php
    $lessons5 = Lessons::getLessonsGroup(5);
    $lessons6 = Lessons::getLessonsGroup(6);
    $lessons7 = Lessons::getLessonsGroup(7);
    $start = 0;
    $lessons_pass = LessonsUsers::getLessonsStatus(5);
    if (!$lessons_pass) {
        $start = 1;
        LessonsUsers::setPassed(5);
    }
    ?>
    <script type="text/javascript">
        $("#ajaxCrudModal").on('shown.bs.modal', function() {
            $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl . '/intro/get-passed' ?>',
                type: 'post',
                data: {
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                    group: 6,
                },
                success: function (data) {
                    if (data.passed==0) {
                        startIntro6();
                        $.ajax({
                            url: '<?php echo Yii::$app->request->baseUrl . '/intro/set-passed' ?>',
                            type: 'post',
                            data: {
                                _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                                group: 6,
                            },
                            success: function (data) {
                                console.log(data.checked);
                            }
                        });
                    }
                }
            });
        });
        $("#ajaxCrudModal").on('hidden.bs.modal', function() {
            $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl . '/intro/get-passed' ?>',
                type: 'post',
                data: {
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                    group: 7,
                },
                success: function (data) {
                    if (data.passed==0) {
                        startIntro7();
                        $.ajax({
                            url: '<?php echo Yii::$app->request->baseUrl . '/intro/set-passed' ?>',
                            type: 'post',
                            data: {
                                _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                                group: 7,
                            },
                            success: function (data) {
                                console.log(data.checked);
                            }
                        });
                    }
                }
            });
        });

        function startIntro5(){
            var intro = introJs();
            intro.setOptions({
                nextLabel:"Дальше",
                prevLabel:"Назад",
                skipLabel:"Пропустить",
                doneLabel:"Понятно",
                exitOnOverlayClick:true,
                disableInteraction:true,
                steps: [
                    <?php
                    foreach ($lessons5 as $lesson)
                        echo '{element: document.querySelectorAll(\'[data-introindex="5-'.$lesson['step'].'"]\')[0],'.
                            'intro: "'.$lesson['hint'].'"},';?>
                    {element: document.querySelectorAll('[data-introindex="5-2"]')[0],
                        intro: "end"},
                ]
            });
            intro.start();
        }
        function startIntro7(){
            var intro = introJs();
            intro.setOptions({
                nextLabel:"Дальше",
                prevLabel:"Назад",
                skipLabel:"Пропустить",
                doneLabel:"Понятно",
                exitOnOverlayClick:true,
                disableInteraction:true,
                steps: [
                    <?php
                    foreach ($lessons7 as $lesson)
                        echo '{element: document.querySelectorAll(\'[data-introindex="7-'.$lesson['step'].'"]\')[0],'.
                            'intro: "'.$lesson['hint'].'"},';?>
                ]
            });
            intro.start();
        }

        $(document).ready(function() {
            var start = <?=$start?>;
            if (start == 1) {
                startIntro5();
            }
        });

        function startLearn(){
            $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl . '/intro/reset-passed' ?>',
                type: 'post',
                data: {
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                    group: 6,
                },
                success: function (data) {
                    console.log(data.checked);
                }
            });
            $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl . '/intro/reset-passed' ?>',
                type: 'post',
                data: {
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                    group: 7,
                },
                success: function (data) {
                    console.log(data.checked);
                }
            });
            startIntro5();
        }

    </script>
