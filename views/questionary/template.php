<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Шаблоны';
/*$this->params['breadcrumbs'][] = 'Курсы';*/

CrudAsset::register($this);

?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>
    <div class="atelier-index">
        <section class="tab-pane" id="ht">
            <h2>   
                Шаблоны 
            </h2>
            <div class="container">
                <?php
                foreach ($templates as $template) {
                    $url = 'http://'. $_SERVER["SERVER_NAME"] . '/images/images.jpg';
                ?>
                    <div class="block-tab">
                        <div class="block-tab-header">
                            <div class="img-frst"><?= '<img style="width:100%; height:140px;" src="'.$url.'">' ?></div>
                        </div>
                        <div class="block-tab-body"> 
                            <h3><?=$template['name']?><br>
                                <span style="font-size:10px;">
                                    <?= Html::a( 'http://' . $_SERVER['SERVER_NAME'] . '/' . $template['link'] , [ '/'.$template['link'] ], ['data-pjax'=>'0', 'title'=> 'Создать', 'target'=>'_blank', ]) ?>
                                </span>
                            </h3>
                            <p>
                                <span class="pull-left">
                                    <?= Html::a('<i style="font-size:20px;" class="glyphicon glyphicon-copy"></i>', [ '/questionary/copy', 'id' => $template['id'], 'copy_from_template' => 1 ], ['role'=>'modal-remote', 'title'=> 'Копировать']) ?>
                                </span>
                                <?= date('H:i:s d.m.Y', strtotime($template['date_up']) ) ?>
                                <?= (!Yii::$app->user->isGuest && Yii::$app->user->identity->type == 0) ?
                                    Html::a(' &nbsp; <i class="glyphicon glyphicon-trash"></i>', ['/questionary/delete', 'id' => $template['id'] ], 
                                    [
                                        'role'=>'modal-remote','title'=>'Удалить',
                                        'class' => 'text-danger',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-toggle'=>'tooltip',
                                        'data-confirm-title'=>'Подтвердите действие',
                                        'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'
                                    ]) : ''
                                ?> 
                            </p>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </section>
    </div>
    <?= (!Yii::$app->user->isGuest && Yii::$app->user->identity->type == 0) ? Html::a('<i class="glyphicon glyphicon-plus" ></i>', ['create', 'is_template' => 1], ['role'=>'modal-remote', 'data-toggle' => 'tooltip', 'data-original-title'=> 'Добавить новый шаблон','class'=>'fix_add']) : '' ?>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
