<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use kartik\time\TimePicker; 
use app\models\Vacancy;

?>

<div class="questionary-form" style="padding: 20px;">
    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
        <div class="col-md-4" data-introindex='4-1'>
            <?= $form->field($model, 'show_in_desktop')->dropDownList($model->getShowList(),['value'=>0]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'access')->dropDownList($model->getAccessList(),[]) ?>
        </div>
        <div class="col-md-4">
            <div style="margin-top: 30px;">
                <?=$form->field($model, 'publish_company')->checkbox()?>                
            </div>
        </div>
    </div>
    <?php if(Yii::$app->user->identity->type == 0 && !$model->isNewRecord) { ?>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'is_template')->checkBox() ?>
            </div>
        </div>
    <?php } ?>

    <?php if(Yii::$app->user->identity->type == 0) { ?>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'user_id')->dropDownList($model->getUsersList(),[]) ?>
            </div>
            
        </div>
    <?php } ?>
    <?php if($model->type == 2) { ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'time_test')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'passage_ball')->textInput(['type' => 'number']) ?>
        </div>
    </div>
    <?php } ?>
    	<div class="row">
            <?php  
                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("SELECT * FROM questionary WHERE is_template = 1 and type = " . $model->type );
                $templates = $command->queryAll();
                foreach ($templates as $value) 
                {
            ?>
    		<div class="col-md-4" style="margin-top: 20px;">
                <div class="panel panel-default">
                    <div class="panel-body template-container" id="template<?=$value['id']?>">
                        <div style="width: 100%; height: 80px; overflow-y:auto;">
                            <h4 style="color: #0055ff;"><?= $value['name']; ?></h4>
                            <?php // $value['description']; ?>
                            
                        </div>
                        <div style="float: right; margin-top:10px;" 
                            onclick="$('#template').val(<?=$value['id']?>); 
                                alert('Успешно выполнено!'); $( '#template<?=$value['id']?>' ).empty(); 
                                var a = '<div style=\'color:#19ff00; font-size: 24px; font-weight:bold; text-align: center;margin-top: 90px;\'>Выбран этот шаблон</div>';
                                $('#template<?=$value['id']?>').append(a); 
                                " class="btn btn-warning btn-xs" >
                            Выбрать шаблон
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    <?= $form->field($model, 'template')->hiddenInput(['id' => 'template'])->label(false) ?>
    <?= $form->field($model, 'step')->hiddenInput(['id'=>'step'])->label(false) ?>
    <?= $form->field($model, 'name')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'vacancy_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'description')->hiddenInput()->label(false) ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>
	  <?php ActiveForm::end(); ?>
</div>