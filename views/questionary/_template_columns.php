<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Vacancy;
use app\models\Users;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\Resume;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'kartik\grid\ExpandRowColumn', 
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return \Yii::$app->controller->renderPartial('_question', ['model'=>$model]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'questionaryCount',
        'content' => function($data){
            $count = Resume::find()->where(['questionary_id' => $data->id])->count();
            return $count;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'vacancy_id',
        'filter' => ArrayHelper::map(Vacancy::find()->all(),'id','name'),
        'content' => function($data){
            return $data->vacancy->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'link',
        'content' => function($data){
            return Html::a( 'http://' . $_SERVER['SERVER_NAME'] . '/' . $data->link , [ '/'.$data->link ], ['data-pjax'=>'0', 'title'=> 'Создать', 'target'=>'_blank', ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'visible' => Yii::$app->user->identity->type == 0,
        'filter' => ArrayHelper::map(Users::find()->all(),'id','fio'),
        'content' => function($data){
            return $data->user->fio . ' ('.$data->user->getTypeDescription().')';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'access',
        'filter' => [ 0 => 'Нет', 1 => 'Да'],
        'content' => function($data){
            return $data->getAccessDesription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'show_in_desktop',
        'filter' => [ 0 => 'Нет', 1 => 'Да'],
        'content' => function($data){
            return $data->getShowDesription();
        }
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'date_cr',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'date_up',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{leadCopy}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'buttons'  => [
            'leadCopy' => function ($url, $model) {
                $url = Url::to(['/questionary/copy', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['role'=>'modal-remote','title'=>'Копировать', 'data-toggle'=>'tooltip']);
            },              
        ],
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
    ],

];   