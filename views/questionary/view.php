<?php

use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Client;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $model app\models\Tests */

\johnitvn\ajaxcrud\CrudAsset::register($this);
$this->title = 'Редактировать';
?>

<div class="panel panel-success" >
    <div class="panel-heading">
        <h3 class="panel-title">
            Анкета
            <span class="pull-right">
                <a class="btn btn-success btn-xs" role="modal-remote" href="<?=Url::toRoute(['/questionary/update', 'id' => $model->id])?>"><i class="glyphicon glyphicon-pencil"></i></a> 
                <a class="btn btn-default btn-xs" data-pjax="0" href="<?=Url::toRoute(['/questionary/index'])?>">Назад</a>
            </span>
        </h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">                
                <?php Pjax::begin(['enablePushState' => false, 'id' => 'questionary-pjax']) ?>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td><b><?=$model->getAttributeLabel('name')?></b></td>
                                    <td><?=Html::encode($model->name)?></td>
                                    <td><b><?=$model->getAttributeLabel('vacancy_id')?></b></td>
                                    <td><?=Html::encode($model->vacancy->name)?></td>
                                </tr>
                                <tr>
                                    <td><b><?=$model->getAttributeLabel('show_in_desktop')?></b></td>
                                    <td><?=Html::encode($model->getShowDesription())?></td>
                                    <td><b><?=$model->getAttributeLabel('link')?></b></td>
                                    <td><?=Html::encode($model->link)?></td>
                                </tr>
                                <tr>
                                    <td><b><?=$model->getAttributeLabel('date_cr')?></b></td>
                                    <td><?=Html::encode($model->date_cr)?></td>
                                    <td><b><?=$model->getAttributeLabel('date_up')?></b></td>
                                    <td><?=Html::encode($model->date_up)?></td>
                                </tr>
                                <tr>
                                    <td><b><?=$model->getAttributeLabel('access')?></b></td>
                                    <td><?=Html::encode($model->getAccessDesription())?></td>
                                    <td><b><?=$model->getAttributeLabel('description')?></b></td>
                                    <td><?=Html::decode($model->description)?></td>
                                </tr>
                               
                            </tbody>
                        </table>
                    </div>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>

<br>
        <div class="row">
            <div class="col-md-12">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'pjax'=>true,
                    'responsiveWrap' => false,
                    'columns' => require(__DIR__.'/_question_columns.php'),
                    'toolbar'=> [
                        ['content'=>
                            Html::a('Создать', ['/questions/create', 'questionary_id' => $model->id],
                                ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary'])
                        ],
                    ],          
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,          
                    'panel' => [
                        'type' => 'primary', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Вопросы',
                        'before'=>'',
                        'after'=>'',
                    ]
                ])?>
            </div>
        </div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "large",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>