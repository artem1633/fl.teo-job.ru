<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;

if (!file_exists('avatars/'.Yii::$app->user->identity->foto) || Yii::$app->user->identity->foto == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.Yii::$app->user->identity->foto;
}

?>
<?php 
        $session = Yii::$app->session;
        if($session['left'] == 'small') $left="x-navigation-minimized";
        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation <?=$left?>">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?=$path?>" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?=$path?>" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?=Yii::$app->user->identity->fio?></div>
                                <div class="profile-data-title"><?=Yii::$app->user->identity->getTypeDescription()?></div>
                            </div>
                            <div class="profile-controls introduction-first" >
                                <?= Html::a('<span class="fa fa-info"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-left','data-introindex'=>'1']); ?>
                                <!-- <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a> -->
                                <!-- <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a> -->
                                <?php // Html::a('<span class="fa fa-envelope"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-right']); ?>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Menu</li>
                    <li class="introduction-first" data-introindex="2" data-intro="Текст2">
                        <?= Html::a('<span class="fa fa-desktop"></span> <span class="xn-text">Показатели</span>', ['/users/dashboard'], []); ?>
                    </li>

                    <li class="introduction-first" data-introindex="3" data-intro="Текст3">
                        <?= Html::a('<span class="fa fa-files-o" style="width: 30px;"> <span class="xn-visible" style="color: #ffffff;font-size: 10px;width:0px;">Результаты</span></span> <span class="xn-text">Заказы</span>', ['/resume/index'], []); ?>
                    </li>
                   <!--  <li class="introduction-first" data-introindex="4" data-intro="Текст4">
                        <?php /*Html::a('<span class="fa fa-file-o" style="width: 30px;"><span class="xn-visible" style="color: #ffffff;font-size: 10px;width:0px;">Форма заказа</span></span> <span class="xn-text">Форма заказа</span>', ['/questionary/index'], []);*/ ?>
                    </li> -->
                    <li class="introduction-first" data-introindex="5" data-intro="Текст5">
                        <?= Html::a('<span class="fa fa-sitemap"></span> <span class="xn-text">Партнерская программа</span>', ['/affiliate-program/index'], []); ?>
                    </li> 
                    <li class="introduction-first" data-introindex="6" data-intro="Текст6">
                        <?= Html::a('<span class="fa fa-credit-card"></span> <span class="xn-text">Финансы</span>', ['/payment/index'], []); ?>
                    </li> 
                    <?php if(Yii::$app->user->identity->type == 0) { ?>                  
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-university"></span> <span class="xn-text">Администратор</span></a>
                            <ul>
                                <li><?= Html::a('<span class="fa fa-users"></span>Пользователи', ['/users/index'], []); ?></li>
                                <li><?= Html::a('<span class="fa fa-cogs"></span>Настройки', ['/settings/index'], []); ?></li>
                                <li><?= Html::a('<span class="fa fa-bookmark"></span>Шаблоны', ['/questionary/index', 'is_template' => 1], []); ?></li>
                                 <li><?= Html::a('<span class=" fa fa-th-large"></span>Страница входа', ['/sliders/index'],[]); ?></li>
                                <li><?= Html::a('<span class=" fa fa-th-large"></span>Инструкции', ['/intro/index'],[]); ?></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <li data-introindex="7">
                        <?= Html::a('<span class="fa fa-cogs" ></span> <span class="xn-text">Настройки</span>', ['/notification/view'], []); ?>
                    </li>
                    <li data-introindex="8" >
                        <?= Html::a('<span class="fa fa-play"></span> <span class="xn-text">Приложение</span>', 'https://play.google.com/store/apps/details?id=ru.teo.job', [ 'target' => '_blank']); ?>
                    </li>
                    <li data-introindex="9" >
                        <?= Html::a('<span class="fa fa-question"></span> <span class="xn-text">Инструкция</span>', ['/users/instruction'], []); ?>
                    </li>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->