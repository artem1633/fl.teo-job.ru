<?php
use Yii,
    yii\helpers\Html,
    yii\helpers\Url,
    yii\bootstrap\Tabs;


$this->title = 'Партнерская программа';

$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?php
echo \yii\bootstrap\Tabs::widget([
    'items' => [
        [
            'label' => 'Информация',
            'content' => $this->render('_description', [
                'description' => $description,
                'company' => $company,
                'settings' => $settings,
            ]),
            'active' => true,
        ],
        [
            'label' => 'Рефералы',
            'content' => $this->render('_referals', [
                'referalsProvider' => $referalsProvider,
                'referalsSearchModel' => $referalsSearchModel,
            ]),
        ],
        [
            'label' => 'Статистика',
            'content' => $this->render('_stats', [
                'retentionProvider' => $retentionProvider,
                'retentionSearchModel' => $retentionSearchModel,
            ]),
        ],
        [
            'label' => 'Выплаты',
            'content' => $this->render('_accounting', [
                'refPaymentsProvider' => $refPaymentsProvider,
                'refPaymentsSearchModel' => $refPaymentsSearchModel,
            ]),
        ],
    ]
]);

?>


