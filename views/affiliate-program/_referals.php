<?php
use yii\helpers\Html,
    app\components\helpers\FunctionHelper,
    app\models\USers,
    kartik\grid\GridView;

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Мои рефералы</h3>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $referalsProvider,
            //'filterModel' => $referalsSearchModel,
            'pjax' => true,
            'responsiveWrap' => false,
            'toolbar' => false,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'columns' => [
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'referal_id',
                    'content' => function ($model) {
                        $referal = Users::findOne($model->referal_id);
                        return $referal->fio;
                    },
                    'visible' => Yii::$app->user->identity->type == 0,
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'fio',
                    'content' => function ($model) {
                        return $model->fio;
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'telephone',
                    'label' => 'Телефон',
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'register_date',
                    'content' => function ($model) {
                        return $model->register_date == null ? '' : date('H:i d.m.Y', strtotime($model->register_date) );
                    }
                ],

                /*[
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'rate_id',
                    'content' => function ($model) {
                        return $model->rate ? $model->rate->name : null;
                    }
                ],*/

            ],
        ]); ?>
    </div>
    <div class="panel-footer">

    </div>
</div>

