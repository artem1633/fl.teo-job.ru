<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Alert */

?>
<div class="alert-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
