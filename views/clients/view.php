<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
?>
<div class="clients-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'name',
            'nik',
            'email:email',
            'skype',
            'additional:ntext',
            'link',
            'phone',
        ],
    ]) ?>

</div>
