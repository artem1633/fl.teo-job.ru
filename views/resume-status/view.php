<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ResumeStatus */
?>
<div class="resume-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'icon',
        ],
    ]) ?>

</div>
