<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Accounts */

?>
<div class="accounts-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
