<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Accounts */
?>
<div class="accounts-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'login',
            'password',
            'user_id',
        ],
    ]) ?>

</div>
