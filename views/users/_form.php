<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\additional\Permissions;

$type = Yii::$app->user->identity->type;
$user_id = Yii::$app->user->identity->id;

?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'telegram_id')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-4">
             <?= $form->field($model, 'new_sms')->dropDownList($model->getNewSmsList(),[]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'telegram_login')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-4"   style="margin-top: 25px;">
                <?=$form->field($model, 'connect_to_telegram')->checkbox()?>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'nik')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'skype')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-12 col-xs-12">
            <?= $form->field($model, 'additional')->textArea(['rows' => 3]) ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'main_balance')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'partner_balance')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'prosent_referal')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'questionary_sum')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'resume_sum')->textInput(['type' => 'number']) ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
