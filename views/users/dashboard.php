<?php

use app\models\Lessons;
use app\models\Settings;
use yii\helpers\Html;
use dosamigos\chartjs\ChartJs;
use app\models\Resume;
use app\models\ResumeStatus; 
use app\models\LessonsUsers;
use yii\bootstrap\Modal;
$this->title = 'Рабочий стол';
?>
<div class="site-index">
    <button  type="button" class="btn btn-xs btn-warning" onclick='startIntro();'>Начать обучение</button>
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6 introduction-first" data-introindex="13" >
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-file"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$questionaryCount?></div>
                    <div class="widget-title">Количество тестов</div>
                    <!-- <div class="widget-subtitle">That visited our site today</div> -->
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6 introduction-first" data-introindex="14" >
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-files-o"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$resumeCount?></div>
                    <div class="widget-title">Количество результатов</div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6 introduction-first" data-introindex="15" >
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-envelope"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$resumeTodayCount?></div>
                    <div class="widget-title">Количество новых результатов сегодня</div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6 introduction-first" data-introindex="16" >
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-send"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$sendCount?></div>
                    <div class="widget-title">Подключен к телеграм</div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

    </div>
    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success panel-hidden-controls ">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">Информация за месяц количество результатов</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                            </ul>                                        
                        </li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>                                
                </div>
                <div class="panel-body introduction-first" data-introindex="17" >
                    
                    <div class="col-md-12">
                        <?= ChartJs::widget([
                            'type' => 'line',
                            'id' => 'lines',
                            'options' => [
                                'class' => 'chartjs-render-monitor',
                                'height' => 80,
                                'width' => 300
                            ],
                            'data' => [
                                'labels' => $days,
                                'datasets' => [
                                    [
                                        'label' => "Количество резюме",
                                        'backgroundColor' => "rgba(179,181,198,0.2)",
                                        'borderColor' => "rgba(179,181,198,1)",
                                        'pointBackgroundColor' => "rgba(179,181,198,1)",
                                        'pointBorderColor' => "#fff",
                                        'pointHoverBackgroundColor' => "#fff",
                                        'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                        'data' => $values
                                    ],
                                    /*[
                                        'label' => "My Second dataset",
                                        'backgroundColor' => "rgba(255,99,132,0.2)",
                                        'borderColor' => "rgba(255,99,132,1)",
                                        'pointBackgroundColor' => "rgba(255,99,132,1)",
                                        'pointBorderColor' => "#fff",
                                        'pointHoverBackgroundColor' => "#fff",
                                        'pointHoverBorderColor' => "rgba(255,99,132,1)",
                                        'data' => [28, 48, 40, 19, 96, 27, 100]
                                    ]*/
                                ]
                            ]
                        ]);
                        ?>
                    </div>

                </div>      
                <div class="panel-footer">
                </div>                            
            </div>
        </div>
    </div>

    <br>
    <br>

    <div class="row">
        <?php 
            foreach ($questionaries as $questionary) {
                $count = Resume::find()->where(['questionary_id' => $questionary->id])->count();
                $statuses = ResumeStatus::find()->all();
                $labels = [];
                $data = [];
                $backgroundColor = [];
                $borderColor = [];
                foreach ($statuses as $status) {
                    $labels [] = $status->name;
                    $data [] = Resume::find()->where(['questionary_id' => $questionary->id, 'status_id' => $status->id])->count();
                    $backgroundColor [] = Resume::getColorCode();
                    $borderColor [] = '#fff';
                }

                $kolvo = Resume::find()->where(['questionary_id' => $questionary->id, 'status_id' => null])->count();
                if($kolvo > 0){
                    $labels [] = 'Статус не дано';
                    $data [] = Resume::find()->where(['questionary_id' => $questionary->id, 'status_id' => null])->count();
                    $backgroundColor [] = Resume::getColorCode();
                    $borderColor [] = '#fff';
                }
        ?>
        <div class="col-md-4 col-xs-12 col-sm-6" style="padding-top: 40px;">
            <div class="panel panel-warning panel-hidden-controls">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title"><?=$questionary->name?></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" onclick=" $.post('/questionary/set-status?id=<?=$questionary->id?>'); " class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>                                
                </div>
                <div class="panel-body" style="height: 350px;">
                    <?php
                        if($count == 0){
                            echo "<h3>На этой анкете нету ни каких резюме</h3>";
                        }
                        else {
                            echo ChartJs::widget([
                                'type' => 'pie',
                                'id' => 'questionary' . $questionary->id,
                                'options' => [
                                    'height' => 225,
                                    'width' => 300
                                ],
                                'data' => [
                                    'labels' => $labels,
                                    'datasets' => [
                                        [
                                            'data' => $data,
                                            'backgroundColor' => $backgroundColor,
                                            'borderColor' => $borderColor,
                                        ],
                                    ]
                                ]
                            ]);
                        }
                    ?>

                </div>      
                <div class="panel-footer">
                </div>                            
            </div>
        </div>
        <?php
            }
        ?>
    </div>
</div>




<?php
$lessons_pass = LessonsUsers::getLessonsStatus(1);
if (!$lessons_pass) {
    $param = Settings::find()->where(['key'=>'instruction_text'])->one();
    Modal::begin([
        'header' => '<h2>Добро пожаловать в TEO FL!</h2>',
        'clientOptions' => ['show' => true],
        'footer' => '<button style="margin-top: -4px;" type="button" class="btn btn-xs btn-warning" '.
            'data-dismiss="modal" onclick=\'startIntro();\'>Начать обучение</button>'
    ]);
    echo $param->text;
    Modal::end();
    LessonsUsers::setPassed(1);
}
$lessons = Lessons::getLessonsGroup(1);
?>
<script type="text/javascript">
    function startIntro(){
        var intro = introJs();
        intro.setOptions({
            nextLabel:"Дальше",
            prevLabel:"Назад",
            skipLabel:"Пропустить",
            doneLabel:"Понятно",
            steps: [
             <?php
                foreach ($lessons as $lesson)
                    echo '{element: document.querySelectorAll(\'[data-introindex="'.$lesson['step'].'"]\')[0],'.
             'intro: "'.$lesson['hint'].'"},';?>

            ]
        });
        intro.setOption("nextLabel", "Дальше").setOption("prevLabel", "Назад").start();
    }
</script>