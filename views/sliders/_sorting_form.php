<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\sortinput\SortableInput;

?>

<div class="scorm-form">

    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-12">
                <?= SortableInput::widget([
                    'name'=> 'columnValues',
                    'items' => $list,
                    'hideInput' => true,
                    'options' => [
                        'class'=>'form-control', 
                        'readonly'=>true
                    ],
                    'sortableOptions' => [
                        'itemOptions'=>['class'=>'alert alert-warning'],
                    ],
                ]);
                ?>
                
            </div>
        </div>


    <?php ActiveForm::end(); ?>
    
</div>
