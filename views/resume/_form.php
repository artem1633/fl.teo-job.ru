<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Resume */
/* @var $form yii\widgets\ActiveForm */
$result = [];
foreach (json_decode($model->tags) as $value) {
    $result [] = $value->id;
}
$model->tags = $result;
?>

<div class="resume-form" style="padding:20px;">

    <?php $form = ActiveForm::begin(); ?>
    
    <div style="display: none;">
        <?= $form->field($model, 'telegram_chat_id')->textInput(['maxlength' => true]) ?>   
        <?= $form->field($model, 'connect_telegram')->dropDownList($model->getConnectList(),[]) ?>
        <?= $form->field($model, 'code')->textInput(['disabled' => true]) ?>   
        <?= $form->field($model, 'new_sms')->dropDownList($model->getNewSmsList(),[]) ?>
        <?= $form->field($model, 'correspondence')->dropDownList($model->getCorrespondenceList(),[]) ?>  
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'fio')->textInput([]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'group_id')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getGroupList(),
                'size' => 'sm',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status_id')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getStatusList(),
                'size' => 'sm',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'budget')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'link_for_project')->textInput([]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'vacancy_id')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getVacancyList(),
                'size' => 'sm',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textArea(['rows' => '2']) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'template_id')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getTemplateList(),
                'size' => 'sm',
                'options' => [
                    'placeholder' => 'Выберите ...',
                    'onchange'=>'
                        $.post( "/resume/sms-text?id='.'"+$(this).val(), function( data ){
                            $( "#sms-text" ).val( data);
                        });
                        ' 
                    ],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'tags')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getTagsList(),
                'size' => 'sm',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                    'multiple' => true,
                ],
            ])?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'text_template')->textArea([ 'rows' => 2, 'id' => 'sms-text']) ?>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'client_id')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getClientList(),
                'size' => 'sm',
                'options' => [
                    'placeholder' => 'Выберите ...',
                    'onchange'=>'
                        $.post( "/resume/nik?id='.'"+$(this).val(), function( data ){
                            $( "#nik" ).val( data);
                        });
                        $.post( "/resume/additional?id='.'"+$(this).val(), function( data ){
                            $( "#additional" ).val( data);
                        });
                        $.post( "/resume/link?id='.'"+$(this).val(), function( data ){
                            $( "#link" ).val( data);
                        });' 
                ],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'client_nik')->textInput(['id' => 'nik']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'client_link')->textInput(['id' => 'link']) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'client_additional')->textArea([ 'rows' => 2, 'id' => 'additional']) ?>
        </div>
    </div>
   
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
