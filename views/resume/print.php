<?php
use yii\bootstrap\ActiveForm;
?>
<html>
<body>
<div class="anketa-body">
    <div class="row">
        <!-- <div class="col-md-12">
            <div class="anketa-title" style="text-align: center;" ><h1><strong><?=$questionary->name?></strong></h1></div>
        </div>
        <div class="col-md-12">
            <?=$questionary->description?>
        </div> -->
        <div class="block">
            <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>
            <?php foreach ($questions as $question) :?>
                <div class="row anketa_form">
                    <?php if ($question) :?>
                        <?=$question;?>
                        <div class="row">
                            <div class="col-md-12">
                                <hr class="line">
                            </div>
                        </div>
                    <?php endif;?>
                </div>
                <?php endforeach;?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</body>
</html>