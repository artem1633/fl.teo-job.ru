<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$session = Yii::$app->session;
?>
<?php $form = ActiveForm::begin(); ?>
    <div class="col-md-3">
        <div class="input-group" style="width:250px;">
            <input type="text" class="form-control" name="name" <?= $post['name'] ? 'value="'.$post['name'].'"' : (isset($_GET['page']) ? 'value="'.$session['name'].'"' : '' ) ?> >
            <div class="input-group-btn">
                <button class="btn btn-info" type="submit"><span class="fa fa-search"></span></button>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
