<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use kartik\sortinput\SortableInput;
use yii\widgets\ActiveForm;
use app\models\Tags;
use app\models\Chat;


CrudAsset::register($this);
$this->title = 'Заказ';
$this->params['breadcrumbs'][] = $this->title;

if($model->avatar == null) $path = 'http://' . $_SERVER['SERVER_NAME'] . '/images/nouser.png';
else $path = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $model->avatar;

$url = Url::to(['/resume/print', 'id' => $model->id]);
$print = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-print"></span></button>', $url, ['data-pjax'=>'0','title'=>'Печать', 'target' => '_blank', 'data-toggle'=>'tooltip']);

$url = Url::to(['/resume/add-resume', 'id' => $model->id]);
$add = Html::a('<button class="btn btn-info btn-xs"><span class="fa fa-magic"></span></button>', $url, ['role'=>'modal-remote','title'=>'Предложить тест', 'data-toggle'=>'tooltip']);

$url = Url::to(['/resume/view', 'id' => $model->id]);
$update = Html::a('<button class="btn btn-info btn-xs" data-introindex="8-6"><span class="glyphicon glyphicon-eye-open"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);

$share = Html::a('<button class="btn btn-warning btn-xs" data-introindex="8-4"><span class="glyphicon glyphicon-share"></span></button>', ['/'.$model->code], ['data-pjax' => '0','title'=>'Поделиться','target'=> "_blank", 'data-toggle'=>'tooltip']);

$url = Url::to(['/resume/delete', 'id' => $model->id]);
$delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url,
    [
        'role'=>'modal-remote','title'=>'Удалить',
        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
        'data-request-method'=>'post',
        'data-toggle'=>'tooltip',
        'data-confirm-title'=>'Подтвердите действие',
        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
    ]);

?>
    <div class="users-page-index">
        <?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>
        <div class="question-container">
            <div class="row">
                <div class="col-md-7">

                    <div class="col-md-12" style="padding-top: 20px;padding-left:0px;padding-right: 0px;">

                        <div class="panel panel-warning">
                            <div class="panel-heading ui-draggable-handle">
                                <h3 class="panel-title"><b>Чат</b></h3>
                                <ul class="panel-controls">
                                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="content-frame-body content-frame-body-left" style="max-height: 400px; overflow: auto; " >
                                    <div class="messages messages-img">
                                        <?php
                                        foreach ($chatText as $value) {
                                            $identity = Yii::$app->user->identity;
                                            if($value->user_id == $identity->id) $in = '';
                                            else $in = 'in';
                                            if (!file_exists('avatars/'.$identity->foto) || $identity->foto == '') {
                                                $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
                                            } else {
                                                $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$identity->foto;
                                            }
                                            ?>
                                            <div class="item <?=$in?> item-visible">
                                                <div class="image">
                                                    <img src="<?=$path?>" alt="<?=$value->user->fio?>">
                                                </div>
                                                <div class="text">
                                                    <div class="heading">
                                                        <a href="#"><?=$value->user->fio?></a>
                                                        <span class="date"><?= date( 'H:i:s d.m.Y', strtotime($value->date_time) ) ?></span>
                                                    </div>
                                                    <?=$value->text?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="panel panel-default push-up-10">
                                    <div class="panel-body panel-body-search">
                                        <?php $form = ActiveForm::begin(); ?>
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button class="btn btn-warning"><span class="fa fa-camera"></span></button>
                                                <button class="btn btn-danger"><span class="fa fa-chain"></span></button>
                                            </div>
                                            <input type="text" name="text" class="form-control" placeholder="Написать сообщение...">
                                            <div class="input-group-btn">
                                                <button class="btn btn-info">Отправить</button>
                                            </div>
                                        </div>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>




                </div>







                <div class="col-md-5">
                    <div class="col-md-12" style="padding-top: 20px;">
                        <div class="panel panel-success panel-hidden-controls" >
                            <div class="panel-heading ui-draggable-handle">
                                <h1 class="panel-title"> <b>Заказ</b> </h1>
                                <?= Html::a('Редактировать &nbsp;<i class="glyphicon glyphicon-pencil"></i>', ['/resume/update', 'id' => $model->id,], [ 'role'=>'modal-remote','title'=> 'Редактировать','class'=>'btn btn-warning btn-rounded pull-right',]) ?>
                                <ul class="panel-controls">
                                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    <li>
                                        <?= Html::a('<span class="fa fa-print"></span>', ['/resume/print', 'id' => $model->id,], [ 'title'=> 'Печать','target' => '_blank','data-pjax'=>"0"]) ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div style="max-height: 600px; overflow: auto; display: flex; justify-content: space-between;">
                                    <div>
                                        <p><b>Название</b> : <?=$model->fio ?></p>
                                        <p><b>Группа</b> : <?=$model->group_id == null ? "<i style='color:red'>(Не задано)</i>" : $model->group->name?></p>
                                        <p><b>Статус</b> : <?=$model->status_id == null ? "<i style='color:red'>(Не задано)</i>" : $model->status->name?></p>
                                        <p>
                                            <b>Теги : </b>
                                        <ul class="list-tags">
                                            <?php
                                            foreach (json_decode($model->tags) as $value) {
                                                $tag = Tags::findOne($value->id);
                                                ?>
                                                <li><a href="#"><span class="fa fa-tag"></span> <?=$tag->name?></a></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                        </p>
                                        <p><b>Бюджет</b> : <?=$model->budget?></p>
                                        <p><b>Клиент</b> : <?=$model->client->name?></p>
                                        <p><b>Шаблон сообщений</b> : <?=$model->template->name?></p>
                                        <p><b>Шаблон текста</b> : <?=$model->text_template?></p>
                                        <p><b>Ссылка на проекта</b> : <?=$model->link_for_project?></p>
                                        <p><b>Описание</b> : <?=$model->description?></p>
                                        <p><b>Тип</b> : <?=$model->vacancy_id == null ? "<i style='color:red'>(Не задано)</i>" : $model->vacancy->name ?></p>
                                       <!--  <p><b>Поделить заказом</b> : <?php /*Html::a('<span class="is-hidden-mobile">https://'. $_SERVER['SERVER_NAME'].'/'.$model->code .'  </span>', ['/'.$model->code], ['data-pjax' => '0','target'=> "_blank"])*/ ?></p> -->

                                    </div>
                                </div>

                            </div>
                            <div class="panel-footer">
                            </div>
                        </div>
                    </div>

                </div>



            </div>
        </div>
        <?php Pjax::end() ?>
    </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php
$chat = Chat::find()->where(['chat_id' => '#resume-'.$model->id, 'is_read' => 0 ])->andWhere([ '!=', 'user_id', Yii::$app->user->identity->id ])->one();
if($chat != null){
    $chat->is_read = 1;
    $chat->save();
}

?>