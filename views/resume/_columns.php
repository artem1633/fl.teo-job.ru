<?php
use yii\helpers\Url;
use app\models\Questionary;
use yii\helpers\ArrayHelper;
use yii\helpers\Arrai;
use app\models\Group;
use app\models\ResumeStatus;
use app\models\Vacancy;
use app\models\Tags;
use yii\helpers\Html;
use app\models\Resume;
use app\models\Questions;
use kartik\select2\Select2;
use kartik\grid\GridView;



return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    /*[
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'contentOptions' =>['data-introindex' => "8-12"],
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return \Yii::$app->controller->renderPartial('_questions', ['model'=>$model]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
        //'width' => '350px',
        'header' => '<div style="min-width:500px;">ФИО / Балы</div>',
        //'header' => 'ФИО / Балы',
        'content' => function($data){
            if($data->connect_telegram == 1) $icon = ' <span class="fa fa-envelope-o"></span>';
            else $icon = '';
            if($data->is_new) $is_new = ' <span style="color:red;" data-introindex="8-3">new</span> ';
            else $is_new = '<span data-introindex="8-3">&nbsp;</span> ';

            $result = [];
            foreach (json_decode($data->tags) as $value) {
                $result [] = $value->id;
            }
            $url = Url::to(['/resume/add-resume', 'id' => $data->id]);
            $add = Html::a('<button class="btn btn-info btn-xs"><span class="fa fa-magic"></span></button>', $url, ['role'=>'modal-remote','title'=>'Предложить тест', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/resume/print', 'id' => $data->id]);
            $print = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-print"></span></button>', $url, ['data-pjax'=>'0','title'=>'Печать', 'target' => '_blank', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/resume/view', 'id' => $data->id]);
            $update = Html::a('<button class="btn btn-info btn-xs" data-introindex="8-6"><span class="glyphicon glyphicon-eye-open"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);

            //$url = Url::to(['/resume/view', 'id' => $data->id]);
            $share = Html::a('<button class="btn btn-warning btn-xs" data-introindex="8-4"><span class="glyphicon glyphicon-share"></span></button>', ['/'.$data->code], ['data-pjax' => '0','title'=>'Поделиться','target'=> "_blank", 'data-toggle'=>'tooltip']);

            $url = Url::to(['/resume/delete', 'id' => $data->id]);
            $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url,
                [
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                ]);
            
            $sales = '';
            if($data->sales !== null) $sales = '<span class="fa fa-ruble"></span> '. $data->sales . '&nbsp; ';
            $url = Url::to(['/resume/view', 'id' => $data->id]);
            return  Html::a('<button class="btn btn-info btn-xs" data-introindex="8-1">'.$data->fio.'</button>',
                    $url, ['data-pjax'=>'0','title'=>'Просмотр', 'target' => '_blank', 'data-toggle'=>'tooltip']) . $icon .
                "  <i class='glyphicon glyphicon-signal' data-introindex=\"8-2\"></i> <span class='success'  >".($data->balls)." ".
                $is_new . '<span class="pull-right">'. $sales. '&nbsp;' . $add . '&nbsp;'  . $print . '&nbsp;'. $update . '&nbsp; '. $share . '&nbsp; ' . $delete .'</span>  <br/>
                    
                    <div class="col-12">
                    <div style="margin-top:10px; padding-left:0px;" data-introindex="8-7" class="col-md-3">' .
                Select2::widget([
                    'name' => 'tags',
                    'data' => Yii::$app->session['tags'],
                    'value' => $result,
                    'size' => 'sm',
                    'options' => [ 'id' => 'tags' . $data->id,'placeholder' => 'Укажите теги ...'],
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true,
                        'multiple' => true,
                    ],
                    'pluginEvents' => [
                        "change" => "function() 
                        {
                            $.get('/resume/set-tags',
                            { 'val' : JSON.stringify($(this).val()), 'id' : $(this).attr('id') },
                                function(data){}
                            );
                        }",
                    ],
                ]) .'</div>
                <div style="margin-top:10px; padding-right:0px;" data-introindex="8-8" class="col-md-3">' . Select2::widget([
                    'name' => 'clients',
                    'data' => Yii::$app->session['clients'],
                    'value' => $data->client_id,
                    'size' => 'sm',
                    'options' => [ 'id' => 'clients' . $data->id,'placeholder' => 'Выберите клиенту...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'tags' => true,
                    ],
                    'pluginEvents' => [
                        "change" => "function() 
                    {
                        $.get('/resume/set-clients',
                        { 'val' : $(this).val(), 'id' : $(this).attr('id'), },
                            function(data){ }
                        );
                    }",
                    ],
                ]) . '</div>
                <div style="margin-top:10px; padding-right:0px;" data-introindex="8-8" class="col-md-3">' . Select2::widget([
                    'name' => 'group',
                    'data' => Yii::$app->session['group'],
                    'value' => $data->group_id,
                    'size' => 'sm',
                    'options' => [ 'id' => 'group' . $data->id,'placeholder' => 'Выберите группу...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'pluginEvents' => [
                        "change" => "function() 
                    {                                        
                        $.get('/resume/set-group',
                        { 'val' : $(this).val(), 'id' : $(this).attr('id') },
                            function(data){ }
                        );
                    }",
                    ],
                ]) . '</div>
                
                '.
                '<div style="margin-top:10px; padding-right:0px;" data-introindex="8-8" class="col-md-3">' . Select2::widget([
                    'name' => 'status',
                    'data' => Yii::$app->session['status'],
                    'value' => $data->status_id,
                    'size' => 'sm',
                    'options' => [ 'id' => 'status' . $data->id,'placeholder' => 'Выберите статус...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'pluginEvents' => [
                        "change" => "function() 
                    {                                        
                        $.get('/resume/set-status',
                        { 'val' : $(this).val(), 'id' : $(this).attr('id') },
                            function(data){ }
                        );
                    }",
                    ],
                ]) . '</div>
                
                </div>
                <div class="col-12">
                    <div style="margin-top:10px; padding-left:0px;" data-introindex="8-7" class="col-md-3">
                    '.
                    \yii\widgets\MaskedInput::widget([
                        'name' => 'data',
                        'value' => $data->budget,
                        'mask' => '9',
                        'options' => [
                            'class' =>'form-control',
                            'placeholder' => 'Укажите бюджет',
                            'onchange'=>"$.get('/resume/change-budget', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                            } ); 
                            ",
                        ],
                        'clientOptions' => ['repeat' => 10, 'greedy' => false]
                    ])
                    .'</div>
                    <div style="margin-top:10px; padding-left:15px;" data-introindex="8-7" class="col-md-9">
                    <input type="text" placeholder="Описание" id="value_playground'.$data->id.'" class="form-control"  value="'.$data->description.'"  onchange="$.get(\'/resume/set-description\', {\'id\':'.$data->id.', \'value\':$(\'#value_playground'.$data->id.'\').val()}, function(data){} );" >
                    </div>
                </div>';
        }
    ],
];