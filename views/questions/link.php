<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Questions;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = $questionary->name;

$questions = Questions::find()->where(['questionary_id' => $questionary->id])->orderBy([ 'ordering' => SORT_ASC])->all();


?>
    <div class="anketa-container lightmode">
        <div class="anketa-box animated fadeInDown">
            <div class="anketa-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="anketa-title" style="text-align: center;" ><strong><?=$questionary->name?></strong></div>
                    </div>
                    <div class="col-md-12" style="color:#fff !important;">
                        <?=$questionary->description?>
                    </div>
                    <div class="block">
                        <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>
                        <?php
                        foreach ($questions as $question)
                        {
                            $image_path = '';
                            foreach (json_decode($question->foto) as $image) {
                                $path = 'https://' . $_SERVER['SERVER_NAME'] . '/' . $image->path;
                                $image_path .= ' <img style="width:120px;height:120px;" src="' . $path . '" >';
                            }

                            if($question->require) $require = '<span style="color:red">*</span>';
                            else $require = '';
                            ?>
                            <div class="row anketa_form">
                                <label class="col-md-12 control-label"><?=$question->name . $require ?> :</label>
                                <div class="col-md-12">
                                    <?= $image_path ?>
                                </div>
                                <label class="col-md-12 control-label"><?= $question->description ?></label>
                                <?php if($question->foto) {?>
                                    <div class="col-md-12">
                                        <?=' <img style="width:120px;height:120px;" src="' . 'https://' . $_SERVER['SERVER_NAME'] . '/uploads/questions/' . $question->id . '/' . $question->foto . '" >'?>
                                    </div>
                                <?php } ?>
                                <?php
                                if($question->type == 0)
                                {
                                    ?>
                                    <div class="col-md-12">
                                        <textarea name="default<?=$question->id?>" <?= $question->require ? 'required="required"' : '' ?> class="form-control" rows="1"></textarea>
                                    </div>
                                    <?php
                                }
                                if($question->type == 1)
                                {
                                    ?>
                                    <div class="col-md-12">
                                        <textarea name="textarea<?=$question->id?>" <?= $question->require ? 'required="required"' : '' ?> class="form-control" rows="1"></textarea>
                                    </div>
                                    <?php
                                }
                                if($question->type == 2)
                                {
                                    ?>
                                    <div class="col-md-12">
                                        <input type="number" name="number<?=$question->id?>" <?= $question->require ? 'required="required"' : '' ?> class="validate[required,maxSize[8] form-control">
                                    </div>
                                    <?php
                                }
                                if($question->type == 3)
                                {
                                    ?>
                                    <?php
                                    $i = 0;
                                    foreach (json_decode($question->individual) as $value) {
                                        $i++;
                                        ?>
                                        <div class="row">
                                            <div class="col-md-1">
                                                <input type="radio" name="radio<?=$question->id?>[]" value="<?=$value->value?>" <?= $question->require ? 'required="required"' : '' ?> style="width:20px;height:20px;" >
                                            </div>
                                            <label style="margin-top: 5px;" class="col-md-11 control-label"><?=$value->value?></label>
                                        </div>
                                    <?php } ?>
                                    <?php
                                }
                                if($question->type == 4)
                                {
                                    ?>
                                    <?php
                                    $i = 0;
                                    foreach (json_decode($question->multiple) as $value) {
                                        $i++;
                                        ?>
                                        <div class="row">
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                                <input type="checkbox" name="checkbox<?=$question->id?>[]" value="<?=$value->question?>" <?= $question->require ? 'required="required"' : '' ?> style=" width:20px;height:20px;" >
                                            </div>
                                            <label style="margin-top: 5px;" class="col-md-11 control-label"><?=$value->question?></label>
                                        </div>
                                    <?php } ?>
                                    <?php
                                }
                                if($question->type == 5)
                                {
                                    ?>
                                    <div class="col-md-12">
                                        <input type="date" name="date<?=$question->id?>" <?= $question->require ? 'required="required"' : '' ?> class="form-control" name="date[]">
                                    </div>
                                <?php       }
                                if($question->type == 6)
                                {
                                    ?>
                                    <div class="col-md-12">
                                        <center>
                                            <div id="poster23_file">
                                                <img style="width:150px;height:150px;" class="" src="<?='http://' . $_SERVER['SERVER_NAME'] . '/images/nouser.png'?>" >
                                            </div>
                                            <input type="file" id="file" name="avatar[]" aria-invalid="false" class="poster23_image" accept="image/*"  >
                                        </center>
                                    </div>
                                    <?php
                                }
                                if($question->type == 7)
                                {
                                    ?>
                                    <div class="col-md-12">
                                        <textarea name="bigtextarea<?=$question->id?>" <?= $question->require ? 'required="required"' : '' ?> class="form-control" rows="6"></textarea>
                                    </div>
                                    <?php
                                }
                                if($question->type == 8)
                                {
                                    ?>
                                    <div class="col-md-12">
                                        <center>
                                            <input type="file" id="many_files" name="many_files[]" multiple aria-invalid="false" class="many_files_image" accept="image/*"  >
                                        </center>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <hr class="line">
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <input type="checkbox" style="display: none;" id="show_in_shop" name="show_in_shop" value="1">
                        <?php
                        $session = Yii::$app->session;
                        //echo "string=" . $session['questionaries'];die;
                        $array = explode(',', '');
                        if( in_array($questionary->id, $array) ){
                            ?>
                            <div class="col-md-12">
                                <div class="text-danger" style="font-weight: bold; text-align: center; font-size: 22px;" >
                                    Вы уже один раз отправили анкету
                                </div>
                            </div>
                            <?php
                        }
                        else {
                            ?>
                            <div class="row anketa_form">
                                <div class="col-md-12">
                                    <button class="btn btn-info" style="width: 100%;" id="checkBtn" type="submit">Отправить</button>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <br>
                                Нажимая "Отправить" Вы соглашаетесть с <?= Html::a('Политика в отношении обработки персональных данных', ['/setting/index'], ['data-pjax'=>'0', 'target' => '_blank' ])?>

                            </div>
                        <?php } ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php
$this->registerJs(<<<JS

$(document).ready(function () {

    var fileCollection = new Array();
    $(document).on('change', '.poster23_image', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:150px;height:150px;" src="'+e.target.result+'"> ';
                $('#poster23_file').html('');
                $('#poster23_file').append(template);
            };
        });
    });

    var requiredCheckboxes = $(':checkbox[required]');
    requiredCheckboxes.on('change', function(e) {
        var checkboxGroup = requiredCheckboxes.filter('[name="' + $(this).attr('name') + '"]');
        //alert('[name="' + $(this).attr('name') + '"]');
        var isChecked = checkboxGroup.is(':checked');
        checkboxGroup.prop('required', !isChecked);
    });
});
JS
);
?>