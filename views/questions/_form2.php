<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */
/* @var $form yii\widgets\ActiveForm */


$individual = 0;
$multiple = 0;

?>

<div class="questions-form" style="padding: 0 20px 0 20px;">

    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'require')->checkBox(['style' => 'margin-top:30px;']) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'view_in_table')->checkBox(['style' => 'margin-top:30px;']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description')->widget(CKEditor::className(),[
                'editorOptions' => [
                    'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                    'height' => '150px',
                ],
            ]);
            ?>
        </div>
        <div class="col-md-12">
            <input id="number" name="number" type="hidden" value="<?=$number?>">
            <input id="path" name="path" type="hidden" value="<?= "uploads/questions/".$number."/" ?>">
            <input id="url" name="url" type="hidden" value="<?= "http://" . $_SERVER['SERVER_NAME'] . '/' ?>">

            <label>Картинки</label>
            <?= \kato\DropZone::widget([
                'options' => [
                     'url'=> Url::to(['/questions/upload-documents', 'number' => $number]),
                     'paramName'=>'documents',
                     //'maxFilesize' => '1',
                     'addRemoveLinks' => true,
                     'dictRemoveFile' => 'Удалить',
                     'dictDefaultMessage' => "Выберите файлы",
                     'acceptedFiles' => 'image/*',
                     'init' => new JsExpression("function(file){ 

                            var resultJsonInput = $('#questions-foto').val();
                            if(resultJsonInput.length > 0) {
                                var resultArray = JSON.parse(resultJsonInput);
                            } else {
                                var resultArray = Array();
                            }

                            var length = resultArray.length;
                            var path = $('#path').val();
                            //alert(path);
                            for (i = 0; i < length; i++) {
                                
                                var file_image = $('#url').val() + resultArray[i].path;
                                //alert(file_image);
                                var mockFile = { name: resultArray[i].name, size: resultArray[i].size, type: resultArray[i].type };
                                this.options.addedfile.call(this, mockFile);
                                this.options.thumbnail.call(this, mockFile, file_image);
                                mockFile.previewElement.classList.add('dz-success');
                                mockFile.previewElement.classList.add('dz-complete');
                            }
     
                     }"),
                ],
                'clientEvents' => [
                     'complete' => "function(file, index){
                         var image = {
                             name: file.name,
                             size: file.size,
                             type: file.type,
                             path: ($('#path').val()+file.name),
                         }; 
                         
                         var resultJsonInput = $('#questions-foto').val();
                         if(resultJsonInput.length > 0) {
                             var resultArray = JSON.parse(resultJsonInput);
                         } else {
                             var resultArray = Array();
                         }
                         resultArray.push(image);
                         var JsonResult = JSON.stringify(resultArray);
                         $('#questions-foto').val(JsonResult);

                        }",
                        'removedfile' => "function(file){
                            $.get('/questions/remove-file',
                            {'id':$('#number').val(), 'filename':file.name},
                                function(data){ 

                                var resultJsonInput = $('#questions-foto').val();
                                if(resultJsonInput.length > 0) {
                                    var resultArray = JSON.parse(resultJsonInput);
                                } else {
                                    var resultArray = Array();
                                }

                                var index = -1;
                                var length = resultArray.length;
                                for (i = 0; i < length; i++) {
                                    if( resultArray[i].name == file.name && resultArray[i].size == file.size && resultArray[i].type == file.type) index = i;
                                }
                                resultArray.splice(index,1);
                                var JsonResult = JSON.stringify(resultArray);
                                $('#questions-foto').val(JsonResult);
                                                    
                                $.get('/questions/set-documents',
                                {'id':$('#number').val(), 'doc':JsonResult},
                                    function(data){ }
                                );

                                }     
                            );
                        }"
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="row" style="padding-top: 20px;" >
        <div class="col-md-12">
            <?= $form->field($model, 'type')->dropDownList($model->getTypeList(),[]) ?>
        </div>
    </div>

    <div class="row" id="text" <?= $model->type != 1  ? 'style="display:none;"' : ''?> >
        <div class="col-md-12">
            <?= $form->field($model, 'text')->textarea(['rows' => 3]) ?>
        </div>
    </div>

    <div class="row" id="number_value" <?= $model->type != 2  ? 'style="display:none;"' : ''?> >
        <div class="col-md-12">
            <?= $form->field($model, 'number')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row" id="individual_answer" <?= $model->type != 3  ? 'style="display:none;"' : ''?> >
        <div class="col-md-12">
            <br>
            <button type="button" class="btn btn-warning btn-xs pull-center" name="add" id="add_input_individual"> + Добавить вариант</button>
            <br>
            <div id="dynamic_individual">
                <?php
                    foreach (json_decode($model->individual) as $value) {
                        $individual++;
                ?>
                <div class="row" id="row<?=$individual?>">
                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <input type="radio" name="individualValue[]" <?=$value->answer ? 'checked="checked"' : '' ?> value="<?=$individual?>" style="margin-top:15px; width:25px;height:25px;" >
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-10"> 
                        <input type="text" class="form-control" name="individual[]" value="<?=$value->value?>" style="margin-top:10px;" /> 
                    </div> 
                    <div class="col-md-1 col-sm-1 col-xs-1" style="margin-left:-20px;" >
                        <button type="button" style="margin-top:10px;" name="remove_individual" id="<?=$individual?>" class="btn btn-danger btn_remove_individual"> <i class="glyphicon glyphicon-trash"></i> 
                        </button> 
                    </div>
                </div>
                <?php } ?>
                <input type="hidden" class="form-control" id="individual_count" name="individual_count" value="<?=$individual?>"/> 
            </div>
        </div>
    </div>

    <div class="row" id="multiple" <?= $model->type != 4  ? 'style="display:none;"' : ''?> >
        <div class="col-md-12">
            <br>
            <button type="button" class="btn btn-warning btn-xs pull-center" name="add" id="add_input_multiple"> + Добавить вариант</button>
            <br>
            <div id="dynamic_multiple">
                <?php
                    foreach (json_decode($model->multiple) as $value) {
                        $multiple++;
                ?>
                <div class="row" id="row<?=$multiple?>">
                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <input type="checkbox" name="multipleValue[]" <?=$value->answer ? 'checked="checked"' : '' ?> value="<?=$multiple?>" style="margin-top:15px; width:25px;height:25px;" >
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-10"> 
                        <input type="text" class="form-control" name="multiple[]" value="<?=$value->question?>" style="margin-top:10px;" /> 
                    </div> 
                    <div class="col-md-1 col-sm-1 col-xs-1" style="margin-left:-20px;" >
                        <button type="button" style="margin-top:10px;" name="remove_multiple" id="<?=$multiple?>" class="btn btn-danger btn_remove_multiple"> <i class="glyphicon glyphicon-trash"></i> 
                        </button> 
                    </div>
                </div>
                <?php } ?>
                <input type="hidden" class="form-control" id="multiple_count" name="multiple_count" value="<?=$multiple?>"/> 
            </div>
        </div>
    </div>


    <div class="row" id="date" <?= $model->type != 5  ? 'style="display:none;"' : ''?> >
        <div class="col-md-12">
            <?= $form->field($model, 'date')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                    //'startView'=>'year',
                    'todayHighlight' => true,
                ]
            ]) ?>
        </div>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'questionary_id')->textInput() ?>   
        <?= $form->field($model, 'foto')->textInput() ?>     
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php 
$this->registerJs(<<<JS
    $('#questions-type').on('change', function() 
    {
        var type = this.value;
        $('#text').hide();
        $('#number_value').hide();
        $('#individual_answer').hide();
        $('#multiple').hide();
        $('#date').hide();

        if(type == 1) {
            $('#text').show();
        }

        if(type == 2) {
            $('#number_value').show();
        }

        if(type == 3) {
            $('#individual_answer').show();
        }

        if(type == 4) {
            $('#multiple').show();
        }

        if(type == 5) {
            $('#date').show();
        }
        
    }
);

$(document).ready(function(){
    var indiv_count = document.getElementById('individual_count').value;
    var multiple_count = document.getElementById('multiple_count').value;

    $('#add_input_individual').click(function(){
    indiv_count++;
    $('#dynamic_individual').append('<div class="row" id="row'+indiv_count+'"><div class="col-md-1 col-sm-1 col-xs-1"><input type="radio" name="individualValue[]" value="'+indiv_count+'" style="margin-top:15px; width:25px;height:25px;" ></div><div class="col-md-10 col-sm-10 col-xs-10"> <input type="text" class="form-control" name="individual[]" style="margin-top:10px;" /> </div> <div class="col-md-1 col-sm-1 col-xs-1" style="margin-left:-20px;" ><button type="button" style="margin-top:10px;" name="remove_individual" id="'+indiv_count+'" class="btn btn-danger btn_remove_individual"> <i class="glyphicon glyphicon-trash"></i> </button> </div></div>');
    });

    $(document).on('click', '.btn_remove_individual', function(){
        var button_id = $(this).attr("id");
        $('#row'+button_id+'').remove();
    });

    $('#add_input_multiple').click(function(){
    multiple_count++;
    $('#dynamic_multiple').append('<div class="row" id="row'+multiple_count+'"><div class="col-md-1 col-sm-1 col-xs-1"><input type="checkbox" name="multipleValue[]" value="'+multiple_count+'" style="margin-top:15px; width:25px;height:25px;" ></div><div class="col-md-10 col-sm-10 col-xs-10"> <input type="text" class="form-control" name="multiple[]" style="margin-top:10px;" /> </div> <div class="col-md-1 col-sm-1 col-xs-1" style="margin-left:-20px;" ><button type="button" style="margin-top:10px;" name="remove_multiple" id="'+multiple_count+'" class="btn btn-danger btn_remove_multiple"> <i class="glyphicon glyphicon-trash"></i> </button> </div></div>');
    });

    $(document).on('click', '.btn_remove_multiple', function(){
        var button_id = $(this).attr("id");
        $('#row'+button_id+'').remove();
    });

});

JS
);
?>