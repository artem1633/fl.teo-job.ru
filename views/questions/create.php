<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Questions */

?>
<div class="questions-create">
    <?= $this->render('_form', [
        'model' => $model,
        'number' => $number,
    ]) ?>
</div>
