<?php

use yii\helpers\Html;
use app\models\Questionary;
use yii\widgets\Pjax;

$_csrf = \Yii::$app->request->getCsrfToken();
$session = Yii::$app->session;

?>      

<div class="anketa-container lightmode">        
    <div class="anketa-box animated fadeInDown">
        <div class="anketa-body" >
	        <h1 style="color: #fff !important;">Анкета: <?=$session['resume']->questionary->name?></h1>
	        <br>
	        <h2 style="color: #fff !important;">Ваш результат: <?=$session['resume']->balls + $model->ball_for_question?> балл</h2>
	        <br>
	        <h2 style="color: #fff !important;">Проходный балл: <?=$session['resume']->questionary->passage_ball?> балл</h2>
	        <br>
	        <h2 style="color: #fff !important;">Время которое ушло на тест: <?=$session['resume']->getTime()?></h2>
	        <br>
	        <h2 style="color: #fff !important;">Сдан: <?=$session['resume']->delivered === 1 ? 'Да' : 'Нет'?></h2>
    	</div>
	</div>
</div>