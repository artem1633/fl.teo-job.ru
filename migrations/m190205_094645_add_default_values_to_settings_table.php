<?php

use yii\db\Migration;

/**
 * Class m190205_094645_add_default_values_to_settings_table
 */
class m190205_094645_add_default_values_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('settings',array(
            'name' => 'ID аккаунта вконтакте для получения уведомлений',
            'key' => 'akk_notify',
            'text' =>'',
        ));

        $this->insert('settings',array(
            'name' => 'Proxy',
            'key' => 'proxy_server',
            'text' =>'148.251.238.124:1080',
        ));

        $this->insert('settings',array(
            'name' => 'Токен для бота VK',
            'key' => 'vk_access_token',
            'text' =>'c63737affb195896217c0a1fec77646b030cbe434c0629e79c8bfea94a3daad8265b07be8b5c8030591ac',
        ));
    }

    public function down()
    {
        
    }
}
