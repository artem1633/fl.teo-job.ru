<?php

use yii\db\Migration;

/**
 * Handles adding description to table `users`.
 */
class m181226_165929_add_description_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'description', $this->text()->comment('Описание компании'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'description');
    }
}
