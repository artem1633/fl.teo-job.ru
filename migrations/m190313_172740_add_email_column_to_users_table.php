<?php

use yii\db\Migration;

/**
 * Handles adding email to table `users`.
 */
class m190313_172740_add_email_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'nik', $this->string(255)->comment('Ник'));
        $this->addColumn('users', 'email', $this->string(255)->comment('Почта'));
        $this->addColumn('users', 'skype', $this->string(255)->comment('Скайп'));
        $this->addColumn('users', 'additional', $this->text()->comment('Дополнительная информация'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'nik');
        $this->dropColumn('users', 'email');
        $this->dropColumn('users', 'skype');
        $this->dropColumn('users', 'additional');
    }
}
