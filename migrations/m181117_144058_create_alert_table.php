<?php

use yii\db\Migration;

/**
 * Handles the creation of table `alert`.
 */
class m181117_144058_create_alert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('alert', [
            'id' => $this->primaryKey(),
            'questionary_id' => $this->integer()->comment('Анкета'),
            'name' => $this->string(255)->comment('Наименование'),
            'count' => $this->float()->comment('Количество'),
            'text' => $this->text()->comment('Текст'),
            'status' => $this->integer()->comment('Статус'),
            'send_date' => $this->datetime()->comment('Дата и время отправки'),
        ]);

        $this->createIndex('idx-alert-questionary_id', 'alert', 'questionary_id', false);
        $this->addForeignKey("fk-alert-questionary_id", "alert", "questionary_id", "questionary", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-alert-questionary_id','alert');
        $this->dropIndex('idx-alert-questionary_id','alert');

        $this->dropTable('alert');
    }
}
