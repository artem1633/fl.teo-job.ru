<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tags`.
 */
class m181115_100044_create_tags_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tags', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(255),
        ]);

        $this->createIndex('idx-tags-user_id', 'tags', 'user_id', false);
        $this->addForeignKey("fk-tags-user_id", "tags", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-tags-user_id','tags');
        $this->dropIndex('idx-tags-user_id','tags');

        $this->dropTable('tags');
    }
}
