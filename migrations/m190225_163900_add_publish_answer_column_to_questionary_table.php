<?php

use yii\db\Migration;

/**
 * Handles adding publish_answer to table `questionary`.
 */
class m190225_163900_add_publish_answer_column_to_questionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questionary', 'publish_answer', $this->boolean()->comment('Публиковать результаты в магазин'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questionary', 'publish_answer');
    }
}
