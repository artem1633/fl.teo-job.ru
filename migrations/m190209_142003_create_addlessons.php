<?php

use yii\db\Migration;

/**
 * Class m190209_142003_create_addlessons
 */
class m190209_142003_create_addlessons extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%lessons}}',['group_id' => 2,'step' => 1,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 3,'step' => 1,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 3,'step' => 2,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 3,'step' => 3,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 3,'step' => 4,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 3,'step' => 5,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 4,'step' => 1,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 4,'step' => 2,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 4,'step' => 3,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 4,'step' => 4,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 4,'step' => 5,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 5,'step' => 1,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 5,'step' => 2,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 5,'step' => 3,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 5,'step' => 4,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 5,'step' => 5,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 6,'step' => 1,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 6,'step' => 2,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 6,'step' => 3,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 6,'step' => 4,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 6,'step' => 5,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 6,'step' => 6,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 6,'step' => 7,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 6,'step' => 8,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 7,'step' => 1,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 1,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 2,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 3,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 4,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 5,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 6,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 7,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 8,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 9,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 10,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 11,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 8,'step' => 12,'hint' => 'Lesson1',]);




    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190209_142003_create_addlessons cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190209_142003_create_addlessons cannot be reverted.\n";

        return false;
    }
    */
}
