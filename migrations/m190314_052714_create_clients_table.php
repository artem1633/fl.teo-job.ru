<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients`.
 */
class m190314_052714_create_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Компания/Пользователь'),
            'name' => $this->string(255)->comment('Наименование'),
            'login' => $this->string(255)->comment('Логин'),
            'password' => $this->string(255)->comment('Пароль'),
        ]);

        $this->createIndex('idx-clients-user_id', 'clients', 'user_id', false);
        $this->addForeignKey("fk-clients-user_id", "clients", "user_id", "users", "id");

        //--------------------------------------------------------------//
        $this->addColumn('resume', 'description', $this->text()->comment('Описание'));
        $this->addColumn('resume', 'budget', $this->float()->comment('Бюджет'));
        $this->addColumn('resume', 'client_id', $this->integer()->comment('Клиент'));

        $this->createIndex('idx-resume-client_id', 'resume', 'client_id', false);
        $this->addForeignKey("fk-resume-client_id", "resume", "client_id", "clients", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //----------------------------------------------------------//
        $this->dropForeignKey('fk-resume-client_id','resume');
        $this->dropIndex('idx-resume-client_id','resume');

        $this->dropColumn('resume', 'description');
        $this->dropColumn('resume', 'budget');
        $this->dropColumn('resume', 'client_id');
        //---------------------------------------
        $this->dropForeignKey('fk-clients-user_id','clients');
        $this->dropIndex('idx-clients-user_id','clients');

        $this->dropTable('clients');

    }
}
