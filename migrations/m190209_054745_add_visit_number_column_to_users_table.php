<?php

use yii\db\Migration;

/**
 * Handles adding visit_number to table `users`.
 */
class m190209_054745_add_visit_number_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'visit_number', $this->integer()->comment('Количество посешений'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'visit_number');
    }
}
