<?php

use yii\db\Migration;

/**
 * Class m181119_113803_delete_values_from_settings_table
 */
class m181119_113803_delete_values_from_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->delete('settings', 
            'id=7'
        );

        $this->delete('settings', 
            'id=8'
        );
    }

    public function down()
    {

    }

}
