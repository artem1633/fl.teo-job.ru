<?php

use yii\db\Migration;

/**
 * Handles adding doptest to table `resume`.
 */
class m190302_153037_add_doptest_column_to_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('resume', 'doptest', $this->integer()->comment('Доп. тест'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('resume', 'doptest');
    }
}
