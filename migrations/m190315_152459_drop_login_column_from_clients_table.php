<?php

use yii\db\Migration;

/**
 * Handles dropping login from table `clients`.
 */
class m190315_152459_drop_login_column_from_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('clients', 'login');
        $this->dropColumn('clients', 'password');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('clients', 'login', $this->string(255));
        $this->addColumn('clients', 'password', $this->string(255));
    }
}
