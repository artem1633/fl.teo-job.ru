<?php

use yii\db\Migration;

/**
 * Handles adding unique_code_for_telegram to table `users`.
 */
class m181114_150013_add_unique_code_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'unique_code_for_telegram', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'unique_code_for_telegram');
    }
}
