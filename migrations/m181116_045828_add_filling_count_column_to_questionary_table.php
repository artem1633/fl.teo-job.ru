<?php

use yii\db\Migration;

/**
 * Handles adding filling_count to table `questionary`.
 */
class m181116_045828_add_filling_count_column_to_questionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questionary', 'filling_count', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questionary', 'filling_count');
    }
}
