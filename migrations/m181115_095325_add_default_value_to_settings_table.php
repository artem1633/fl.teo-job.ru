<?php

use yii\db\Migration;

/**
 * Class m181115_095325_add_default_value_to_settings_table
 */
class m181115_095325_add_default_value_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('settings',array(
            'name' => 'Для оповещения',
            'key' => 'notifications',
            'text' =>'',
        ));
    }

    public function down()
    {

    }
}
