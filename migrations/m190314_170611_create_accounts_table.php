<?php

use yii\db\Migration;

/**
 * Handles the creation of table `accounts`.
 */
class m190314_170611_create_accounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('accounts', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'login' => $this->string(255)->comment('Логин'),
            'password' => $this->string(255)->comment('Пароль'),
            'user_id' => $this->integer()->comment('Пользователь/Компания'),
        ]);

        $this->createIndex('idx-accounts-user_id', 'accounts', 'user_id', false);
        $this->addForeignKey("fk-accounts-user_id", "accounts", "user_id", "users", "id");

        $this->createIndex('idx-resume-user_id', 'resume', 'user_id', false);
        $this->addForeignKey("fk-resume-user_id", "resume", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-accounts-user_id','accounts');
        $this->dropIndex('idx-accounts-user_id','accounts');

        $this->dropTable('accounts');

        $this->dropForeignKey('fk-resume-user_id','resume');
        $this->dropIndex('idx-resume-user_id','resume');
    }
}
