<?php

use yii\db\Migration;

/**
 * Handles dropping fit from table `resume`.
 */
class m190314_142619_drop_fit_column_from_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('resume', 'fit');
        $this->dropColumn('resume', 'mark');
        $this->dropColumn('resume', 'ball_for_question');
        $this->dropColumn('resume', 'doptest');
        $this->dropColumn('resume', 'show_in_shop');

        $this->addColumn('resume', 'link', $this->string(255));
        $this->addColumn('resume', 'link_for_project', $this->string(255));

        $this->addColumn('clients', 'nik', $this->string(255)->comment('Ник'));
        $this->addColumn('clients', 'email', $this->string(255)->comment('Почта'));
        $this->addColumn('clients', 'skype', $this->string(255)->comment('Скайп'));
        $this->addColumn('clients', 'additional', $this->text()->comment('Дополнительная информация'));
        $this->addColumn('clients', 'link', $this->string(255)->comment('Ссылка на профиль'));
        $this->addColumn('clients', 'phone', $this->string(255)->comment('Телефон'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('resume', 'fit', $this->integer());
        $this->addColumn('resume', 'mark', $this->integer());
        $this->addColumn('resume', 'ball_for_question', $this->integer());
        $this->addColumn('resume', 'doptest', $this->integer());
        $this->addColumn('resume', 'show_in_shop', $this->integer());

        $this->dropColumn('resume', 'link');
        $this->dropColumn('resume', 'link_for_project');

        $this->dropColumn('clients', 'nik');
        $this->dropColumn('clients', 'email');
        $this->dropColumn('clients', 'skype');
        $this->dropColumn('clients', 'additional');
        $this->dropColumn('clients', 'link');
        $this->dropColumn('clients', 'phone');
    }
}
