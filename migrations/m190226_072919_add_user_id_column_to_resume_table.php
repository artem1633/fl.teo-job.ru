<?php

use yii\db\Migration;

/**
 * Handles adding user_id to table `resume`.
 */
class m190226_072919_add_user_id_column_to_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('resume', 'user_id', $this->integer()->comment('Пользователь'));
        $this->addColumn('resume', 'buyed', $this->integer()->comment('Куплено'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('resume', 'user_id');
        $this->dropColumn('resume', 'buyed');
    }
}
