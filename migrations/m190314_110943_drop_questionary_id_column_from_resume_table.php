<?php

use yii\db\Migration;

/**
 * Handles dropping questionary_id from table `resume`.
 */
class m190314_110943_drop_questionary_id_column_from_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-resume-questionary_id','resume');
        $this->dropIndex('idx-resume-questionary_id','resume');

        $this->dropColumn('resume', 'questionary_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('resume', 'questionary_id', $this->integer());

        $this->createIndex('idx-resume-questionary_id', 'resume', 'questionary_id', false);
        $this->addForeignKey("fk-resume-questionary_id", "resume", "questionary_id", "questionary", "id");
    }
}
