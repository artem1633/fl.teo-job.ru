<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sms_template`.
 */
class m190317_060649_create_sms_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sms_template', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'description' => $this->text()->comment('Описание'),
            'user_id' => $this->integer()->comment('Пользователь'),
        ]);

        $this->createIndex('idx-sms_template-user_id', 'sms_template', 'user_id', false);
        $this->addForeignKey("fk-sms_template-user_id", "sms_template", "user_id", "users", "id");

        //---------------------------------------------------------------------------------------//
        $this->addColumn('resume', 'template_id', $this->integer()->comment('Шаблон сообщений'));
        $this->addColumn('resume', 'text_template', $this->text()->comment('Текст сообщений'));

        $this->createIndex('idx-resume-template_id', 'resume', 'template_id', false);
        $this->addForeignKey("fk-resume-template_id", "resume", "template_id", "sms_template", "id");

        $this->dropColumn('resume', 'link');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-resume-template_id','resume');
        $this->dropIndex('idx-resume-template_id','resume');

        $this->dropColumn('resume', 'template_id');
        $this->dropColumn('resume', 'text_template');
        
        //-------------------------------------------------------------------------------------//
        $this->dropForeignKey('fk-sms_template-user_id','sms_template');
        $this->dropIndex('idx-sms_template-user_id','sms_template');

        $this->dropTable('sms_template');

        $this->addColumn('resume', 'link', $this->string(255)->comment('Ссылка'));

    }
}
