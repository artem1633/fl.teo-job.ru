<?php

use yii\db\Migration;

/**
 * Handles adding foto to table `users`.
 */
class m181111_055921_add_foto_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'foto', $this->string(255)->comment('Фото'));
        $this->addColumn('users', 'utm', $this->string(255)->comment('UTM'));
        $this->addColumn('users', 'agree', $this->boolean()->comment('Я согласен с обработкой персональных данных'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'foto');
        $this->dropColumn('users', 'utm');
        $this->dropColumn('users', 'agree');
    }
}
