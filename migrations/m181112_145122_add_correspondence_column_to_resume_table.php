<?php

use yii\db\Migration;

/**
 * Handles adding correspondence to table `resume`.
 */
class m181112_145122_add_correspondence_column_to_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('resume', 'correspondence', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('resume', 'correspondence');
    }
}
