<?php

namespace app\controllers;

use app\modules\api\controllers\BotinfoController;
use Yii;
use app\models\Resume;
use app\models\ResumeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Questions;
use app\models\Tags;
use app\models\Chat;
use app\models\Questionary;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;
use app\models\AccountingReport;
use app\models\Clients;
use app\models\SmsTemplate;

/**
 * ResumeController implements the CRUD actions for Resume model.
 */
class ResumeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {     
        $request = Yii::$app->request;
        $post = $request->post();
        $searchModel = new ResumeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $post);
        $dataProviderExport = $searchModel->search(Yii::$app->request->queryParams, $post);

        $dataProviderExport->pagination = ['pageSize' => 1000000,];
        Yii::$app->session['dataProvider'] = $dataProviderExport;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'post' => $post,
        ]);
    }

    public function actionShop()
    {     
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM questionary WHERE publish_answer = 1");
        $old = $command->queryAll();
        $questionaryId = [];
        foreach ($old as $value) {
            $questionaryId [] = $value['id'];
        }

        $request = Yii::$app->request;
        $post = $request->post();
        $searchModel = new ResumeSearch();
        $dataProvider = $searchModel->searchShop(Yii::$app->request->queryParams, $questionaryId, $post);

        return $this->render('shop', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'post' => $post,
        ]);
    }



    /**
     * Displays a single Resume model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $active = [];
        $chat = new Chat();
        $chat->chat_id = '#resume-'.$id;
        $model = $this->findModel($id);
        $model->new_sms = 0;
        $model->is_new = 0;
        $model->save();
        $textQuestionsCount = 0;

        if ( $request->post() ) {
            if($request->post()['text'] != ''){
                $chat->text = $request->post()['text'];
                $chat->save();
                if ($model->connect_telegram) {
                    BotinfoController::getReq('sendMessage', ['chat_id' => $model->telegram_chat_id, 'parse_mode'=>'HTML', 'text' => $request->post()['text']]);
                }

            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Resume::find(),
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => array('pageSize' => 20),
        ]);

        $chatText = Chat::find()->where(['chat_id' => '#resume-'.$id ])->all();
        return $this->render('view', [
            'model' => $model,
            'active' => $active,
            'chatText' => $chatText,
            'textQuestionsCount' => $textQuestionsCount,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddResume($id) 
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title'=> "Предложить тест",
            'size' => 'normal',
            'content'=>$this->renderAjax('add-resume', [
                'model' => $model,
            ]),
        ];
    }

    public function actionGetlink($resume_id, $id)
    {
        $questionary = Questionary::findOne($id);
        return 'https://' . $_SERVER['SERVER_NAME'] . '/' .$questionary->link . '?doptest='.$resume_id;
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Resume();       

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){

            $result = [];
            foreach ($request->post()['Resume']['tags'] as $value) {
                $tag = Tags::findOne($value);
                if($tag == null){
                    $tag = new Tags();
                    $tag->name = $value;
                        
                    if ($tag->save()) $result [] = [
                        'id' => $tag->id,
                    ];
                }else{
                    $result [] = [
                        'id' => $tag->id,
                    ];
                }
            }

            $model->tags = json_encode($result);
            $model->save();

            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "Резюме",
                'size' => 'large',
                'forceClose'=>true,
            ];    
        }else{
             return [
                'title'=> "Редактировать",
                'size' => 'large',
                'content'=>$this->renderAjax('create', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];        
        }
    }

    /**
     * Updates an existing Resume model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){

                $result = [];
                foreach ($request->post()['Resume']['tags'] as $value) {
                    $tag = Tags::findOne($value);
                    if($tag == null){
                        $tag = new Tags();
                        $tag->name = $value;
                        
                        if ($tag->save()) $result [] = [
                            'id' => $tag->id,
                        ];
                    }else{
                        $result [] = [
                            'id' => $tag->id,
                        ];
                    }
                }

                $model->tags = json_encode($result);
                $model->save();

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Резюме",
                    'size' => 'large',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Редактировать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];        
            }
        }else{ 
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionBuyResume($id)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM resume WHERE id = " . $id);
        $model = $command->queryOne();
        $command = $connection->createCommand("SELECT * FROM questionary WHERE id = " . $model['questionary_id']);
        $questionary = $command->queryOne();
        $command = $connection->createCommand("SELECT * FROM users WHERE id = " . $questionary['user_id']);
        $user = $command->queryOne();

        if(Yii::$app->user->identity->main_balance < $questionary['result_resume_sum']){
            return [
                'title' => 'Покупка',
                'size' => 'normal',
                'content'=> '<span class="text-danger" style="font-size:20px;font-weight:bold;">У вас не хватает средства</span>',
                'footer'=> Html::button('ОК',['class'=>'btn btn-primary','data-dismiss'=>"modal"])
            ];
        }

        $newResume = new Resume();
        $newResume->fit = $model['fit'];
        $newResume->category = $model['category'];
        $newResume->is_new = $model['is_new'];
        $newResume->mark = $model['mark'];
        $newResume->telegram_chat_id = $model['telegram_chat_id'];
        $newResume->new_sms = $model['new_sms'];
        $newResume->values = $model['values'];
        $newResume->correspondence = $model['correspondence'];
        $newResume->fio = $model['fio'];
        $newResume->tags = $model['tags'];
        $newResume->balls = $model['balls'];
        $newResume->ball_for_question = $model['ball_for_question'];
        $newResume->avatar = $model['avatar'];
        $newResume->ip = $model['ip'];
        $newResume->time_spent = $model['time_spent'];
        $newResume->delivered = $model['delivered'];
        $newResume->sales = null;
        $newResume->show_in_shop = $model['show_in_shop'];
        $newResume->user_id = Yii::$app->user->identity->id;
        $newResume->buyed = 1;
        $newResume->save();

        Yii::$app->db->createCommand()->update('users', ['main_balance' => Yii::$app->user->identity->main_balance - $questionary['result_resume_sum'] ], [ 'id' => Yii::$app->user->identity->id ])->execute();
        Yii::$app->db->createCommand()->update('resume', ['sales' => $model['sales'] + $questionary['result_resume_sum'] ], [ 'id' => $model['id'] ])->execute();
        Yii::$app->db->createCommand()->update('users', ['partner_balance' => (float)$user['partner_balance'] + (float)$questionary['result_resume_sum'] ], [ 'id' => $user['id'] ])->execute();

        $pay = new AccountingReport();
        $pay->date_report = date('Y-m-d H:i:s');
        $pay->company_id = Yii::$app->user->identity->id;
        $pay->operation_type = 6;
        $pay->amount = $questionary['result_resume_sum'];
        $pay->description = 'Покупка резюме';
        $pay->save();

        /*$pay = new AccountingReport();
        $pay->date_report = date('Y-m-d H:i:s');
        $pay->company_id = Yii::$app->user->identity->id;
        $pay->operation_type = 6;
        $pay->amount = $questionary['result_resume_sum'];
        $pay->description = 'Покупка резюме';
        $pay->save();*/

        $text = 'Купили результат в системе '. $model['fio'];
        $report = new AccountingReport([
            'date_report' => date('Y-m-d H:i:s'),
            'company_id' => $user['id'],
            'operation_type' => AccountingReport::TYPE_INCOME_AFFILIATE,
            'amount' => floatval($questionary['result_resume_sum']),
            'description' => $text,
        ]);
        $report->save();
        BotinfoController::sendadmin(['chat_id' => '247187885', 'parse_mode'=>'HTML', 'text' => $text]);

        return [
            'title' => 'Покупка',
            'forceReload'=>'#crud-datatable-pjax',
            'size' => 'normal',
            'content'=> 'Успешно выполнено!',
            'footer'=> Html::button('ОК',['class'=>'btn btn-primary','data-dismiss'=>"modal"])
        ];    
        
    }

    public function actionSetGroup($val, $id)
    {
        $strlen = strlen( $id );
        $numeric = ''; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $id, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric .= $char;            
        }

        $resume = $this->findModel($numeric);
        $resume->group_id = $val;
        $resume->is_new = 0;
        $resume->save();
    }

    public function actionSetClients($val, $id)
    {
        $strlen = strlen( $id );
        $numeric = ''; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $id, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric .= $char;            
        }

        $resume = $this->findModel($numeric);
        $client = Clients::find()->where(['id' => $val])->one();
        if($client == null){
            $client = new Clients();
            $client->name = $val;
            $client->save();
        }
        $resume->client_id = $client->id;
        $resume->save();
    }

    public function actionChangeBudget($id, $value)
    {
        $resume = $this->findModel($id);
        $resume->budget = $value;
        $resume->save();
    }

    public function actionSetDescription($id, $value)
    {
        $resume = $this->findModel($id);
        $resume->description = $value;
        $resume->save();
    }

    public function actionSetStatus($val, $id)
    {
        $strlen = strlen( $id );
        $numeric = ''; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $id, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric .= $char;            
        }
         
        $resume = $this->findModel($numeric);
        $resume->status_id = $val;
        $resume->is_new = 0;
        $resume->save();
    }

    public function actionSetTags($val, $id)
    {
        $array = json_decode($val);
        $result = [];
        foreach ($array as $value) {
            $tag = Tags::findOne($value);
            if($tag == null){
                $tag = new Tags();
                $tag->name = $value;
                        
                if ($tag->save()) $result [] = [
                    'id' => $tag->id,
                ];
            }else{
                $result [] = [
                    'id' => $tag->id,
                ];
            }
        }

        $strlen = strlen( $id );
        $numeric = ''; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $id, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric .= $char;            
        }
         
        $resume = $this->findModel($numeric);
        $resume->tags = json_encode($result);
        $resume->save();
    }

    public function actionExport()
    { 
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");

        $dataProvider = Yii::$app->session['dataProvider'];
        $questionaries = [];
        foreach ($dataProvider->getModels() as $model) {
            $questionaries [] = $model->questionary_id;
        }

        $questionaries = array_unique($questionaries);
        $work_sheet = -1;
        foreach ($questionaries as $questionaryId) {
            $work_sheet++;
            $questionary = Questionary::findOne($questionaryId);
            $resumes = Resume::find()->where(['questionary_id' => $questionaryId])->all();
            $objWorkSheet = $objPHPExcel->createSheet($work_sheet);
            $objWorkSheet->setTitle($questionary->name);
            $questions = Questions::find()->where(['questionary_id' => $questionaryId])->orderBy(['ordering' => SORT_ASC])->all();
            $index = -1;
            foreach ($questions as $questionOne) {
                $index++;
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow($index, 1, $questionOne->name);
                
                $rowCount = 1;
                foreach ($resumes as $resume) {
                    $rowCount++;
                    foreach (json_decode($resume->values) as $value) {
                        if($value->question == $questionOne->id){
                            $question = Questions::findOne($value->question);
                            if($question != null){
                                $answer = '';
                                if($question->type == 3){
                                    foreach (json_decode($question->individual) as $individual) {
                                        foreach (json_decode($value->value) as $val) {
                                            if($val->key == $individual->value) $answer .= $individual->value;
                                        }
                                    }
                                }
                                else{
                                    if($question->type == 4){
                                        foreach (json_decode($question->multiple) as $multiple) {
                                            foreach (json_decode($value->value) as $val) {
                                                if($val->key == $multiple->question) $answer .= $multiple->question . '; ';
                                            }
                                        }
                                    }
                                    else $answer .= $value->value;
                                }
                                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow($index, $rowCount, $answer);
                            }
                        }
                    }
                }
            }
            $objPHPExcel->getActiveSheet()->getStyle("A1:".$objPHPExcel->getActiveSheet()->getHighestDataColumn()."1")->getFont()->setBold(true);
        }

        $filename='Eksport'.'.xlsx'; //save our workbook as this file name
        /*header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');*/

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0');
        $objWriter->setPreCalculateFormulas(false);
        $objWriter->save('php://output');

        //без этой строки при открытии файла xlsx ошибка!!!!!!
        exit;
    }

    /**
     * Delete an existing Resume model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Resume model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }
 
    /**
     * Finds the Resume model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Resume the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Resume::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPrint($id)
    {
        $active = [];
        $resume = $this->findModel($id);
        foreach (json_decode($resume->values) as $value) {
            $question = Questions::findOne($value->question);
            if($question != null){
                $name = $question->getSelectesItemPrint($value->value, $model->ball_for_question);
                $active += [
                    $question->id => $name,
                ];
                if($question->type != 3 && $question->type != 4) $textQuestionsCount++;
            }
        }
        //$headers = Yii::$app->response->headers;
        //$headers->add('Content-Type', 'application/pdf');
        $this->layout='print';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $this->render('print',['questions'=>$active,]),
            'cssFile' => 'css/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.img-circle {border-radius: 50%;}',
            'options' => [
                'title' => 'Teo-JOB',
                'subject' => 'PDF'
            ],
            'methods' => [
                'SetHeader'=>['Сделано с помощью сервиса https://teo-job.ru'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionNik($id)
    {
        $client = Clients::findOne($id);
        return $client->nik;
    }
    public function actionLink($id)
    {
        $client = Clients::findOne($id);
        return $client->link;
    }

    public function actionAdditional($id)
    {
        $client = Clients::findOne($id);
        return $client->additional;
    }

    public function actionSmsText($id)
    {
        $sms = SmsTemplate::findOne($id);
        return $sms->description;
    }
}
