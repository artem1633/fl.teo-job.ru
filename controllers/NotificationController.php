<?php

namespace app\controllers;

use Yii;
use app\models\Notification;
use app\models\NotificationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\VacancySearch;
use app\models\GroupSearch;
use app\models\ResumeStatusSearch;
use app\models\TagsSearch;
use app\models\ClientsSearch;
use app\models\AccountsSearch;
use app\models\SmsTemplateSearch;

/**
 * NotificationController implements the CRUD actions for Notification model.
 */
class NotificationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new NotificationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Notification model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {   
        $request = Yii::$app->request;

        $notificationModel = new NotificationSearch();
        $notDataProvider = $notificationModel->search(Yii::$app->request->queryParams);

        $vacancyModel = new VacancySearch();
        $vacancyProvider = $vacancyModel->search(Yii::$app->request->queryParams);

        $groupModel = new GroupSearch();
        $groupProvider = $groupModel->search(Yii::$app->request->queryParams);

        $resumeModel = new ResumeStatusSearch();
        $resumeProvider = $resumeModel->search(Yii::$app->request->queryParams);

        $tagsModel = new TagsSearch();
        $tagsProvider = $tagsModel->search(Yii::$app->request->queryParams);

        $clientsModel = new ClientsSearch();
        $clientsProvider = $clientsModel->search(null);

        $accountsModel = new AccountsSearch();
        $accountsProvider = $accountsModel->search(Yii::$app->request->queryParams);

        $templateModel = new SmsTemplateSearch();
        $templateProvider = $templateModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'notDataProvider' => $notDataProvider,
            'vacancyModel' => $vacancyModel,
            'vacancyProvider' => $vacancyProvider,
            'groupModel' => $groupModel,
            'groupProvider' => $groupProvider,
            'resumeModel' => $resumeModel,
            'resumeProvider' => $resumeProvider,
            'tagsModel' => $tagsModel,
            'tagsProvider' => $tagsProvider,
            'clientsModel' => $clientsModel,
            'clientsProvider' => $clientsProvider,
            'accountsModel' => $accountsModel,
            'accountsProvider' => $accountsProvider,
            'templateModel' => $templateModel,
            'templateProvider' => $templateProvider,
        ]);
    }

    /**
     * Updates an existing Notification model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#notification-pjax',
                    'title'=> "Уведомление",
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Finds the Notification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
